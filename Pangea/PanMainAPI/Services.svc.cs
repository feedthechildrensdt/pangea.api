﻿using System;
using System.Collections.Generic;
using System.Data;
using PanMainAPI.Classes;
using PanMainAPI.DAO;
using PanMainAPI.Data;

using PanMainAPI.Data.DataModels;

namespace PanMainAPI
{
    public class FieldServices : IFieldServices
    {
        BLO BusinessLogic = new BLO();
        Admin Admin = new Admin();
        Enrollment Enroll = new Enrollment();
        Update Update = new Update();
        CodeTables Codes = new CodeTables();
        MarkLocation Mark = new MarkLocation();
        Workflow Workflow = new Workflow();
        SystemFunctions SysFunc = new SystemFunctions();
        ChildRecord childRecord = new ChildRecord();

        /// <summary>
        /// Get script(s) to update the local DB for the field app.
        /// </summary>
        public DataSet GetDBversion(DBVersion CurrentDB)
        {
            return SysFunc.GetVersionDB(CurrentDB);
        }

        public List<DBConfigObject> GetDBConfig(string UserID)
        {
            return SysFunc.GetDBConfig(UserID);
        }

        #region CodeTables
        /// <summary>
        /// Get a data set with all of the code tables in it.
        /// </summary>
        public DataSet GetCodeTables(string UserID)
        {
            return Codes.GetCodeTables(UserID);
        }

        /// <summary>
        /// Manage code table values
        /// </summary>
        public ReturnMessage SaveCodeValue(CodeTableValue CodeValue, string UserID)
        {
            ReturnMessage msg = new ReturnMessage("0", "No Result", "Nothing was processed");

            switch (CodeValue.CodeTable)
            {
                case "Child_Remove_Reason":
                    msg = Codes.Child_Remove_Reason(CodeValue, UserID);
                    break;
                case "Gender":
                    msg = Codes.Gender(CodeValue, UserID);
                    break;
                case "Lives_With":
                    msg = Codes.Lives_With(CodeValue, UserID);
                    break;
                case "Personality_Type":
                    msg = Codes.Personality_Type(CodeValue, UserID);
                    break;
                case "Chore":
                    msg = Codes.Chore(CodeValue, UserID);
                    break;
                case "Favorite_Activity":
                    msg = Codes.Favorite_Activity(CodeValue, UserID);
                    break;
                case "Favorite_Learning":
                    msg = Codes.Favorite_Learning(CodeValue, UserID);
                    break;
                case "Grade_Level":
                    msg = Codes.Grade_Level(CodeValue, UserID);
                    break;
                case "Content_Type":
                    msg = Codes.Content_Type(CodeValue, UserID);
                    break;
                case "Country":
                    msg = Codes.Country(CodeValue, UserID);
                    break;
                case "Location":
                    msg = Codes.Location(CodeValue, UserID);
                    break;
                case "Region":
                    msg = Codes.Region(CodeValue, UserID);
                    break;
                case "Decline_Reason":
                    msg = Codes.DeclineSubReason_Editor(CodeValue, UserID);
                    break;
            }

            return msg;
        }

        /// <summary>
        /// Get list of code table values
        /// </summary>
        public List<CodeTableValue> GetCodeTableValues(string CodeTable, string UserID)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();

            switch (CodeTable)
            {
                case "Child_Remove_Reason":
                    values = Codes.Get_Child_Remove_Reason(UserID);
                    break;
                case "Gender":
                    values = Codes.Get_Gender(UserID);
                    break;
                case "Lives_With":
                    values = Codes.Get_Lives_With(UserID);
                    break;
                case "Personality_Type":
                    values = Codes.Get_Personality_Type(UserID);
                    break;
                case "Chore":
                    values = Codes.Get_Chore(UserID);
                    break;
                case "Favorite_Activity":
                    values = Codes.Get_Favorite_Activity(UserID);
                    break;
                case "Favorite_Learning":
                    values = Codes.Get_Favorite_Learning(UserID);
                    break;
                case "Grade_Level":
                    values = Codes.Get_Grade_Level(UserID);
                    break;
                case "Content_Type":
                    values = Codes.Get_Content_Type(UserID);
                    break;
                case "Country":
                    values = Codes.Get_Country(UserID);
                    break;
                case "Location":
                    values = Codes.Get_Location(UserID);
                    break;
                case "Region":
                    values = Codes.Get_Region(UserID);
                    break;

                case "Decline_Reason":
                    values = Codes.Get_DeclineSubReasons(UserID);
                    break;

            }

            return values;
        }
        #endregion

        #region Admin
        /// <summary>
        /// Remove child and add reason
        /// </summary>
        public ReturnMessage RemoveAChild(RemoveChild Child, string UserID)
        {
            return Admin.RemoveChild(Child, UserID);
        }

        /// <summary>
        /// Get all enrolled children
        /// </summary>
        public List<ListChild> GetChildren(string userid, int countryid, int locationid, int childnumber)
        {
            return Admin.GetChildren(userid, countryid, locationid, childnumber);
        }

        /// <summary>
        /// Get a enrolled child record
        /// </summary>
        public AdminChild GeteChild(string childid, string userid)
        {
            return Admin.GeteChild(childid, userid);
        }

        /// <summary>
        /// Get an enrolled child record file
        /// </summary>
        public List<DownLoadFile> GetFile(DownLoadFile file)
        {
            return Admin.GetFile(file);
        }

        /// <summary>
        /// Save and update to an enrolled child ADMIN ONLY
        /// </summary>
        public List<ReturnMessage> SaveChild(AdminChild child, string userid)
        {
            return Admin.SaveChild(child, userid);
        }
        #endregion

        #region MarkLocation
        /// <summary>
        /// This function is to mark a location for update by scheduling to have the eledgible children from dbo moved to Pending.
        /// </summary>
        public ReturnMessage MarkLocationForUpdate(string UserID, string LocationID, DateTime VisitDate, int LeadDays, DateTime DueDate)
        {
            return Mark.MarkLocation4Update(UserID, LocationID, VisitDate, LeadDays, DueDate);
        }

        /// <summary>
        /// This function is to Get all mark a location for update by scheduling to have the eledgible children from dbo moved to Pending.
        /// </summary>
        public List<MarkLocationForUpdateValue> GetLocationForUpdate(string UserID, string LocationID, string countryID)
        {
            return Mark.GetLocation4Update(UserID, LocationID, countryID);
        }

        public ReturnMessage CancelLocationForUpdate(string UserID, string LocationCalendarID)
        {
            return Mark.CancelLocation4Update(UserID, LocationCalendarID);
        }

        public ReturnMessage EditLocationForUpdate(string UserID, string LocationID, int LocationCalendarID, DateTime VisitDate, int LeadDays, DateTime DueDate)
        {
            return Mark.EditLocation4Update(UserID, LocationID, LocationCalendarID, VisitDate, LeadDays, DueDate);
        }
        #endregion

        #region Enrollment
        /// <summary>
        /// Take the child data provided and runs the Enrollment process of the Business rules are met.
        /// </summary>
        public List<ReturnMessage> EnrollAChild(string ChildID, string UserID)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Enroll.EnrollTheChild(ChildID, UserID);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }

        /// <summary>
        /// Saves the information in the provided child for future enfollment.
        /// </summary>
        public List<ReturnMessage> SaveEnrollment(EnrollChild Child, string UserID)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Enroll.SaveTheChild(Child, UserID);
            ReturnMessages.AddRange(SysFunc.CheckForDuplicates(Child, UserID));

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }

        /// <summary>
        /// Upload a supporting file for an Enrolled child.
        /// </summary>
        public List<ReturnMessage> UploadAnEnrollmentFile(UploadFile File)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();

            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Enroll.UploadEnrollmentFile(File);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);

            return ReturnMessages;
        }

        /// <summary>
        /// Get the incomplete enrollments that have been saved by a user
        /// </summary>
        public List<ListChild> GetIncompleteEnrollments(string UserID, int CountryID, int LocationID, int ActionID)
        {
            return Enroll.GetIncompleteEnrollments(UserID, CountryID, LocationID, ActionID);
        }

        /// <summary>
        /// Get the saved child record for a incomplete enrollment
        /// </summary>
        public EnrollChild GetIncompleteChildEnrollment(string ChildID, string UserID)
        {
            return Enroll.GetIncompleteChildEnrollment(ChildID, UserID);
        }

        /// <summary>
        /// Get a enrollment file
        /// </summary>
        public List<DownLoadFile> GetEnrollmentFile(DownLoadFile photo)
        {
            return Enroll.GetEnrollmentFile(photo);
        }

        /// <summary>
        /// Delete Enrollment
        /// </summary>
        public List<ReturnMessage> DeleteEnrollment(string ChildID, string UserID)
        {
            return Enroll.DeleteAnEnrollment(ChildID, UserID);
        }

        /// <summary>
        /// Delete a supporting file for an Enrolled child.
        /// </summary>
        public List<ReturnMessage> DeleteAnEnrollmentFile(UploadFile File)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Enroll.DeleteEnrollmentFile(File);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }
        #endregion

        #region Update
        /// <summary>
        /// Take the child data provided and runs the Pending process of the Business rules are met.
        /// </summary>
        public List<ReturnMessage> SubmitUpdate(string ChildID, string UserID)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Update.SubmitUpdate(ChildID, UserID);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }

        /// <summary>
        /// Saves the information in the provided for the Update.
        /// </summary>
        public List<ReturnMessage> SaveUpdate(UpdateChild Child, string UserID)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Update.SaveUpdate(Child, UserID);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }

        /// <summary>
        /// Upload a supporting file for an update child.
        /// </summary>
        public List<ReturnMessage> UploadAnUpdateFile(UploadFile File)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Update.UploadUpdateFile(File);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }

        /// <summary>
        /// Get the incomplete Pendings that have been saved by a user
        /// </summary>
        public List<ListChild> GetIncompleteUpdates(string UserID, int CountryID, int LocationID, int ActionID)
        {
            return Update.GetIncompleteUpdates(UserID, CountryID, LocationID, ActionID);
        }

        /// <summary>
        /// Get the saved child record for a incomplete Pending
        /// </summary>
        public List<AdminChild> GetIncompleteChildUpdate(string ChildID, string UserID)
        {
            List<AdminChild> Child = new List<AdminChild>();
            Child.Add(Update.GetIncompleteUpdate(ChildID, UserID));
            Child.Add(Update.GeteUpdate(ChildID, UserID));
            return Child;
        }

        /// <summary>
        /// Delete Pending record
        /// </summary>
        public List<ReturnMessage> DeleteUpdate(string ChildID, string UserID, int RemoveReasonID)
        {
            return Update.DeleteAnUpdate(ChildID, UserID, RemoveReasonID);
        }

        /// <summary>
        /// Get the saved child record for an update
        /// </summary>
        public List<AdminChild> GetFieldAppUpdateChildren(string UserID, string LocationID)
        {
            return Update.ChildUpdateInformation(UserID, LocationID);
        }

        /// <summary>
        /// Get the child Profile photo
        /// </summary>
        public ChildProfilePhoto GetChildProfilePhoto(ChildProfilePhoto photo)
        {
            return Update.GetProfilePhoto(photo);
        }

        /// <summary>
        /// Get a uploaded file
        /// </summary>
        public List<DownLoadFile> GetUpdateFile(DownLoadFile photo)
        {
            return Update.GetUpdateFile(photo);
        }

        /// <summary>
        /// Delete a supporting file for an update child.
        /// </summary>
        public List<ReturnMessage> DeleteAnUpdateFile(UploadFile File)
        {
            List<ReturnMessage> ReturnMessages = new List<ReturnMessage>();
            ReturnMessage msg = new ReturnMessage("-1", "ERROR", "There has been an issue with the API. Please contact Service Desk.");

            ReturnMessages = Update.DeleteUpdateFile(File);

            if (ReturnMessages.Count < 1)
                ReturnMessages.Add(msg);
            return ReturnMessages;
        }
        #endregion

        #region WorkFlow
        /// <summary>
        /// Get the open workflows
        /// </summary>
        public DataSet GetWorkFlows(int WorkFlowID, int CountryID, int LocationID, string UserID, string QCApprove)
        {
            return Workflow.GetWorkFlows(WorkFlowID, CountryID, LocationID, UserID, QCApprove);
        }

        /// <summary>
        /// Get the workflow steps for a given instance
        /// </summary>
        public List<WorkFlowStep> GetChildWorkFlow(int PendingChildID, int WorkFlowID, string UserID, bool QCApprover)
        {
            return Workflow.GetChildWorkFlow(PendingChildID, WorkFlowID, UserID, QCApprover);
        }

        /// <summary>
        /// Save a workflow step
        /// </summary>
        public ReturnMessage SaveWorkFlow(WorkFlowStep WorkFlow, string UserID)
        {
            return Workflow.SaveWorkFlow(WorkFlow, UserID);
        }

        /// <summary>
        /// Add a workflow step such as a decline
        /// </summary>
        public ReturnMessage AddSubWorkFlow(SubWorkFlow WorkFlow, string UserID)
        {
            return Workflow.AddSubWorkFlow(WorkFlow, UserID);
        }

        /// <summary>
        /// This is to decline a step for the provided reason. 
        /// </summary>
        public ReturnMessage DeclineStep(Decline Decline, string UserID)
        {
            return Workflow.DeclineStep(Decline, UserID);
        }

        /// <summary>
        /// Update the information for a declined record.
        /// </summary>
        public List<ReturnMessage> UpdateDecline(AdminChild Child, string UserID)
        {
            return Workflow.DeclineUpdate(Child, UserID);
        }

        /// <summary>
        /// Get a list of translation workflows for the given country
        /// </summary>
        public DataSet GetTranslationWorkFlows(int ActionReason, int CountryID, int LocationID, int LanguageID, string UserID)
        {
            return Workflow.GetTranslationWorkFlows(ActionReason, CountryID, LocationID, LanguageID, UserID);
        }

        /// <summary>
        /// Get the translations for a given child
        /// </summary>
        public List<TranslationStep> GetChildTranslations(string ChildID, int ChildNumber, string UserID)
        {
            return Workflow.GetChildTranslationWorkFlow(ChildID, ChildNumber, UserID);
        }

        /// <summary>
        /// Get a list of declines that need to be worked
        /// </summary>
        public DataSet GetDeclineList(int WorkFlowID, int ChildNum, string ChildID, bool PendingOnly, string UserID, bool QCApprover, int CountryID, int LocationID)
        {
            return Workflow.GetDeclineList(WorkFlowID, ChildNum, ChildID, PendingOnly, UserID, QCApprover, CountryID, LocationID);
        }

        /// <summary>
        /// Get the declines for a given child
        /// </summary>
        public List<DeclineDetails> GetChildDeclines(string ChildID, int ChildNumber, bool PendingOnly, string UserID, bool QCApprover)
        {
            return Workflow.GetChildDecline(ChildID, ChildNumber, PendingOnly, UserID, QCApprover);
        }

        /// <summary>
        /// Save a the translated mle
        /// </summary>
        public ReturnMessage SaveTranslatedMLE(string UserID, string ChildID, int LanguageCodeID, string MajorLifeDescription, int ChildMajorLifeEventID, int ChildMajorLifeEventLangID)
        {
            return Workflow.SaveTranslatedMLE(UserID, ChildID, LanguageCodeID, MajorLifeDescription, ChildMajorLifeEventID, ChildMajorLifeEventLangID);
        }
        #endregion

        #region Child Records
        public List<ReturnMessage> SaveChildRecord(AdminChild AdChild, string UserID)
        {
            return childRecord.SaveChildRecord(AdChild, UserID);
        }

        public List<ReturnMessage> UploadChildRecordFile(UploadFile File)
        {
            return childRecord.UploadChildRecordFile(File);
        }

        public List<ListChild> GetChildRecords(string UserID, int CountryID, int LocationID/*, int ActionID*/)
        {
            return childRecord.GetChildRecords(UserID, CountryID, LocationID);
        }

        public List<DownLoadFile> GetChildRecordFile(DownLoadFile File)
        {
            return childRecord.GetChildRecordFile(File);
        }
        #endregion


        /// <summary>
        /// Get a country records for data set with all of the code tables in it.
        /// </summary>
        public List<Country_DropDown_Item> GetCountryRecords(string UserID)
        {

            SPROCer sprocer = new SPROCer();

            return sprocer.GetCountryRecords(UserID);

        }


       

        /// <summary>
        /// Get a country records for data set with all of the code tables in it.
        /// </summary>
        public List<DropDown_Item_w_ID> Get_Locations_For_User(string UserID, int Country_ID)
        {

            SPROCer sprocer = new SPROCer();

            List<DropDown_Item_w_ID> the_data = sprocer.Get_Locations_For_User(UserID, Country_ID);

            return the_data;

        }


        public List<ReturnMessage> Send_Content_File
            (
            string UserID,
            UploadFile File,
            Settingz.enum_Operation_Type e_Operation_Type,
            Settingz.enum_Content_Binary_Type e_Content_Binary_Type
            )
        {

            return Content_Files.Process_Content_File_Upload
                (
                UserID, 
                File, 
                e_Operation_Type, 
                e_Content_Binary_Type
                );

        }


        /// <summary>
        /// Get listing of Decline Reasons
        /// </summary>
        public List<Decline_Reason_Codes> DeclineReasonCodes(string UserID)
        {
            return CodeTables.GetDeclineReasonCodes(UserID);
        }

        /// <summary>
        /// Get listing of Decline Reasons
        /// </summary>
        public List<Decline_Sub_Reason_Codes> DeclineSubReasonCodes(string UserID, int DeclineReasonCodeID)
        {
            return CodeTables.GetDeclineSubReasonCodes(UserID, DeclineReasonCodeID);
        }


    }


}
