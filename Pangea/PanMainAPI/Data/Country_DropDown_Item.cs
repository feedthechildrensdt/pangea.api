﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Runtime.Serialization;


namespace PanMainAPI.Data
{
    /// <summary>
    /// Used to return data for a single Country DropDown List item
    /// </summary>
    [DataContract]
    public class Country_DropDown_Item
    {

        [DataMember]
        public int Country_ID { get; set; }

        [DataMember]
        public string Country_Name { get; set; }

    }



}