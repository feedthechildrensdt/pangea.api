﻿using System;
using System.Runtime.Serialization;

namespace PanMainAPI.Data
{
    [DataContract]
    public class DBConfigObject
    {
        public DBConfigObject()
        {
            Active = false;
            Config_ID = 0;
            Name = String.Empty;
            Description = String.Empty;
            Bit_Value = null;
            Int_Value = null;
            Text_Value = null;
            Dec_Value = null;
            Datetime_Value = null;
        }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public int Config_ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool? Bit_Value { get; set; }

        [DataMember]
        public long? Int_Value { get; set; }

        [DataMember]
        public string Text_Value { get; set; }

        [DataMember]
        public Decimal? Dec_Value { get; set; }

        [DataMember]
        public DateTime? Datetime_Value { get; set; }
    }
}