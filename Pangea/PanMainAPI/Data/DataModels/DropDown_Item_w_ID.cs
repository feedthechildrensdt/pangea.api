﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Runtime.Serialization;


namespace PanMainAPI.Data.DataModels
{

    /// <summary>
    /// Used to return data for a single DropDown List item with ID value
    /// </summary>
    [DataContract]
    public class DropDown_Item_w_ID
    {

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string DropDown_Caption { get; set; }

    }

}