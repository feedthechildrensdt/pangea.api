﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;

namespace PanMainAPI.Data
{

    [DataContract]
    public class Decline_Sub_Reason_Codes
    {

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public int Action_ID { get; set; }

        [DataMember]
        public Guid Action_User { get; set; }

        [DataMember]
        public DateTime Action_Date { get; set; }

        [DataMember]
        public int? Action_Reason { get; set; }
        
        [DataMember]
        public int Decline_SubReason_Code_ID { get; set; }

        [DataMember]
        public int Decline_Reason_Code_ID { get; set; }

        [DataMember]
        public string Decline_SubReason { get; set; }

        [DataMember]
        public string Description { get; set; }

    }


}