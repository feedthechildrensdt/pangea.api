﻿using System.Runtime.Serialization;

namespace PanMainAPI
{
    /// <summary>
    /// Used to create a Sub WorkFlow such as Decline
    /// </summary>
    [DataContract]
    public class SubWorkFlow
    {
        [DataMember]
        public int Pending_Child_ID { get; set; }

        [DataMember]
        public int Workflow_Code_ID { get; set; }

        [DataMember]
        public int Child_Workflow_Step_ID { get; set; }

        [DataMember]
        public int File_ID { get; set; }

        public SubWorkFlow(int childid, int workflowid, int instanceid, int fileid)
        {
            Pending_Child_ID = childid;
            Workflow_Code_ID = workflowid;
            Child_Workflow_Step_ID = instanceid;
            File_ID = fileid;
        }

        public SubWorkFlow()
        {

        }
    }

    /// <summary>
    /// Is a step in a workflow such as QC Child Information
    /// </summary>
    [DataContract]
    public class WorkFlowStep
    {
        [DataMember]
        public int Pending_Child_ID { get; set; }

        [DataMember]
        public int Workflow_Code_ID { get; set; }

        [DataMember]
        public int Step_Code_ID { get; set; }

        [DataMember]
        public string Step_Name { get; set; }

        [DataMember]
        public int Child_Workflow_Step_ID { get; set; }

        [DataMember]
        public int File_ID { get; set; }

        [DataMember]
        public int Status_Code_ID { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public int SubOrder { get; set; }

        [DataMember]
        public bool QCApprover { get; set; }

        [DataMember]
        public bool Complete { get; set; }

        [DataMember]
        public int Workflow_Response_to_Step { get; set; }

        public WorkFlowStep(int childid, int workflowid, int stepid, string stepname, int instanceid, int fileid, int statusid, int order, int suborder, int responsestep)
        {
            Pending_Child_ID = childid;
            Workflow_Code_ID = workflowid;
            Step_Code_ID = stepid;
            Step_Name = stepname;
            Child_Workflow_Step_ID = instanceid;
            File_ID = fileid;
            Status_Code_ID = statusid;
            Order = order;
            SubOrder = suborder;
            Workflow_Response_to_Step = responsestep;
            QCApprover = true;
            Complete = true;
        }

        public WorkFlowStep(int childid, int workflowid, int stepid, string stepname, int instanceid)
        {
            Pending_Child_ID = childid;
            Workflow_Code_ID = workflowid;
            Step_Code_ID = stepid;
            Step_Name = stepname;
            Child_Workflow_Step_ID = instanceid;
            QCApprover = true;
            Complete = true;
        }

        public WorkFlowStep()
        {
            Step_Name = "";
            QCApprover = true;
            Complete = true;
        }
    }

    /// <summary>
    /// Is a decline to a WorkFlowStep with the needed info
    /// </summary>
    [DataContract]
    public class Decline
    {
        [DataMember]
        public int Child_Workflow_Step_ID { get; set; }

        [DataMember]
        public int Decline_Reason_Code { get; set; }

        [DataMember]
        public int Decline_SubReason_Code { get; set; }

        [DataMember]
        public int Workflow_Response { get; set; }

        [DataMember]
        public string Decline_Note { get; set; }

        public Decline(int workflowid, int reasoncode, int subreasoncode, int responsestep)
        {
            Child_Workflow_Step_ID = workflowid;
            Decline_Reason_Code = reasoncode;
            Decline_SubReason_Code = subreasoncode;
            Workflow_Response = responsestep;
        }

        public Decline()
        {

        }
    }

    /// <summary>
    /// Is a translation needed for a child
    /// </summary>
    [DataContract]
    public class TranslationStep
    {
        [DataMember]
        public int WorkflowStepID { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public int ChildNumber { get; set; }

        [DataMember]
        public int WorkflowID { get; set; }

        [DataMember]
        public int StepCodeID { get; set; }

        [DataMember]
        public string Step { get; set; }

        [DataMember]
        public int FileID { get; set; }

        [DataMember]
        public int MLEID { get; set; }

        [DataMember]
        public int MLELangID { get; set; }

        [DataMember]
        public int LangCode { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string StreamID { get; set; }

        public TranslationStep(int workflowstep, string firstanme, string lastname, string childid, int childnum, string step, int fileid, int mleid, int workflowid, int stepcodeid, int mlelang, int langcode, string description, string streamid)
        {
            int WorkflowStepID = workflowstep;
            string FirstName = firstanme;
            string LastName = lastname;
            string ChildID = childid;
            int ChildNumber = childnum;
            string Step = step;
            int FileID = fileid;
            int MLEID = mleid;
            int MLELangID = mlelang;
            int WorkflowID = workflowid;
            int StepCodeID = stepcodeid;
            int LangCode = langcode;
            string Description = description;
            string StreamID = streamid;
        }

        public TranslationStep(string childid, int childnum)
        {
            int WorkflowStepID = 0;
            string FirstName = "";
            string LastName = "";
            string ChildID = childid;
            int ChildNumber = childnum;
            string Step = "";
            int FileID = 0;
            int MLEID = 0;
            int MLELangID = 0;
            int WorkflowID = 0;
            int StepCodeID = 0;
            int LangCode = 0;
            string Description = "";
            string StreamID = "";
        }

        public TranslationStep()
        {
            int WorkflowStepID = 0;
            string FirstName = "";
            string LastName = "";
            string ChildID = "";
            int ChildNumber = 0;
            string Step = "";
            int FileID = 0;
            int MLEID = 0;
            int MLELangID = 0;
            int WorkflowID = 0;
            int StepCodeID = 0;
            int LangCode = 0;
            string Description = "";
            string StreamID = "";
        }
    }

    /// <summary>
    /// Is the information on a decline to a WorkFlowStep
    /// </summary>
    [DataContract]
    public class DeclineDetails
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public int ChildNumber { get; set; }

        [DataMember]
        public int PendingChildID { get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Workflow { get; set; }

        [DataMember]
        public string Step { get; set; }

        [DataMember]
        public int FileID { get; set; }

        [DataMember]
        public string DeclineReason { get; set; }

        [DataMember]
        public string DeclineSubReason { get; set; }

        [DataMember]
        public string DeclineNotes { get; set; }

        [DataMember]
        public int ChildWorkflowStepID { get; set; }

        public DeclineDetails(string childid, int childnum, int pendingid, int id, string workflow, string step, int fileid, string declinereason, string declinesubreason, string declinenote, int workflowstepid)
        {
            ChildID = childid;
            ChildNumber = childnum;
            PendingChildID = pendingid;
            ID = id;
            Workflow = workflow;
            Step = step;
            FileID = fileid;
            DeclineReason = declinereason;
            DeclineSubReason = declinesubreason;
            DeclineNotes = declinenote;
            ChildWorkflowStepID = workflowstepid;
        }

        public DeclineDetails()
        {

        }
    }
}