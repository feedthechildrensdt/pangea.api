﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace PanMainAPI
{
    /// <summary>
    /// This is the object used to define a file that is to be uploaded for a child.
    /// </summary>
    [DataContract]
    public class UploadFile
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public int FileID { get; set; }

        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public byte[] FileBytes { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public int ContentTypeID { get; set; }

        [DataMember]
        public int FileTypeID { get; set; }

        [DataMember]
        public string FileStreamID { get; set; }

        [DataMember]
        public string PrimaryStreamID { get; set; }

        [DataMember]
        public FileMetaData MetaData { get; set; }

        public UploadFile(string childid, string uploaduser)
        {
            ChildID = childid;
            UserID = uploaduser;
            MetaData = new FileMetaData();
        }

        public UploadFile(string childid, int fileType, byte[] fileBytes, string uploaduser)
        {
            ChildID = childid;
            FileTypeID = fileType;
            FileBytes = fileBytes;
            UserID = uploaduser;
            MetaData = new FileMetaData();
        }

        public UploadFile(string childid, int fileType, byte[] fileBytes, FileMetaData metadata, string uploaduser)
        {
            ChildID = childid;
            FileTypeID = fileType;
            FileBytes = fileBytes;
            MetaData = metadata;
            UserID = uploaduser;
        }
    }

    /// <summary>
    /// This is the object used to define a file that is to be downloaded for a child.
    /// </summary>
    [DataContract]
    public class DownLoadFile
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public byte[] FileBytes { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public int ContentTypeID { get; set; }

        [DataMember]
        public int FileTypeID { get; set; }

        [DataMember]
        public string FileStreamID { get; set; }

        [DataMember]
        public string PrimaryStreamID { get; set; }

        [DataMember]
        public int FileID { get; set; }

        public DownLoadFile(string childid, string uploaduser)
        {
            ChildID = childid;
            UserID = uploaduser;
        }

        public DownLoadFile(string childid, int fileType, byte[] fileBytes, string uploaduser)
        {
            ChildID = childid;
            FileTypeID = fileType;
            FileBytes = fileBytes;
            UserID = uploaduser;
        }

        public DownLoadFile(int fileid)
        {
            FileID = fileid;
        }
    }

    /// <summary>
    /// This is the object used to define a file's meta data that is to be uploaded for an image.
    /// </summary>
    [DataContract]
    public class FileMetaData
    {
        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string Software { get; set; }

        [DataMember]
        public string DateTaken { get; set; }

        [DataMember]
        public string Original { get; set; }

        [DataMember]
        public string Digitized { get; set; }

        [DataMember]
        public string GPSVersionID { get; set; }

        [DataMember]
        public string GPSLatitudeRef { get; set; }

        [DataMember]
        public string GPSLatitude { get; set; }

        [DataMember]
        public string GPSLongitudeRef { get; set; }

        [DataMember]
        public string GPSLongitude { get; set; }

        [DataMember]
        public string GPSAltitudeRef { get; set; }

        [DataMember]
        public string GPSAltitude { get; set; }

        [DataMember]
        public string GPSTimeStamp { get; set; }

        [DataMember]
        public string GPSImgDirectionRef { get; set; }

        [DataMember]
        public string GPSDateStamp { get; set; }

        public FileMetaData()
        {
        }

        public FileMetaData(string make, string model, string software, string datetaken, string original, string digitized, string gpsversionid, string gpslatituderef, string gpslatitude, string gpslongituderef, string gpslongitude, string gpsaltituderef, string gpsaltitude, string gpstimestamp, string gpsimgdirectionref, string gpsdatestamp)
        {
            Make = make;
            Model = model;
            Software = software;
            DateTaken = datetaken;
            Original = original;
            Digitized = digitized;
            GPSVersionID = gpsversionid;
            GPSLatitudeRef = gpslatituderef;
            GPSLatitude = gpslatitude;
            GPSLongitudeRef = gpslongituderef;
            GPSLongitude = gpslongitude;
            GPSAltitudeRef = gpsaltituderef;
            GPSAltitude = gpsaltitude;
            GPSTimeStamp = gpstimestamp;
            GPSImgDirectionRef = gpsimgdirectionref;
            GPSDateStamp = gpsdatestamp;
        }

        public FileMetaData(string make, string model, string software, string datetaken, string original, string digitized)
        {
            Make = make;
            Model = model;
            Software = software;
            DateTaken = datetaken;
            Original = original;
            Digitized = digitized;
        }
    }

    /// <summary>
    /// This is the object used to define a file that is to be downloaded for a child.
    /// </summary>
    [DataContract]
    public class ChildProfilePhoto
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public byte[] FileBytes { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public int ContentTypeID { get; set; }

        [DataMember]
        public int FileTypeID { get; set; }

        [DataMember]
        public string FileStreamID { get; set; }

        [DataMember]
        public string PrimaryStreamID { get; set; }

        public ChildProfilePhoto(string childid, string uploaduser)
        {
            ChildID = childid;
            UserID = uploaduser;
        }
    }

    
}