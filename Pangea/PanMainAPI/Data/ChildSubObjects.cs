﻿using System;
using System.Runtime.Serialization;

namespace PanMainAPI
{
    /// <summary>
    /// This is the object used to remove the child from the DB, deactivate.
    /// </summary>
    [DataContract]
    public class RemoveChild
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public int RemovalReasonID { get; set; }

        public RemoveChild(string childid, int removalreason)
        {
            ChildID = childid;
            RemovalReasonID = removalreason;
        }
    }


    /// <summary>
    /// This is the object used to display a list of children to select from.
    /// </summary>
    [DataContract]
    public class ListChild
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public string ChildNumber { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string OptionalName { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string ActionType { get; set; }

        [DataMember]
        public string ActionReason { get; set; }

        public ListChild()
        { }

        public ListChild(string childid, string childnumber, string location, string firstname, string middlename, string lastname, string optionalname, DateTime dateofbirth, string gender)
        {
            ChildID = childid;
            ChildNumber = childnumber;
            Location = location;
            FirstName = firstname;
            MiddleName = middlename;
            LastName = lastname;
            OptionalName = optionalname;
            DateOfBirth = dateofbirth;
            Gender = gender;
        }
    }
}