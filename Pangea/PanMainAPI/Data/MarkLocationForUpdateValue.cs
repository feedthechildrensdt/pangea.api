﻿using System;
using System.Runtime.Serialization;

namespace PanMainAPI
{
    [DataContract]
    public class MarkLocationForUpdateValue
    {
        [DataMember]
        public string LocationCalendarID { get; set; }

        [DataMember]
        public string RegionCode { get; set; }

        [DataMember]
        public string RegionName { get; set; }

        [DataMember]
        public string CountryCodeID { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string LocationCodeID { get; set; }

        [DataMember]
        public string LocationName { get; set; }

        [DataMember]
        public DateTime VisitDate { get; set; }

        [DataMember]
        public string LeadDays { get; set; }

        [DataMember]
        public DateTime DueDate { get; set; }
    }
}