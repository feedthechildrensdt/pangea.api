﻿using PanMainAPI.Data;
using PanMainAPI.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PanMainAPI
{
    [ServiceContract]
    public interface IFieldServices
    {
        /// <summary>
        /// Get script(s) to update the local DB for the field app.
        /// </summary>
        [OperationContract]
        DataSet GetDBversion(DBVersion CurrentDB);

        [OperationContract]
        List<DBConfigObject> GetDBConfig(string UserID);

        #region CodeTables
        /// <summary>
        /// Get a data set with all of the code tables in it.
        /// </summary>
        [OperationContract]
        DataSet GetCodeTables(string UserID);

        /// <summary>
        /// Manage code table values
        /// </summary>
        [OperationContract]
        ReturnMessage SaveCodeValue(CodeTableValue CodeValue, string UserID);

        /// <summary>
        /// Get list of code table values
        /// </summary>
        [OperationContract]
        List<CodeTableValue> GetCodeTableValues(string CodeTable, string UserID);
        #endregion

        #region Admin
        /// <summary>
        /// Remove child and add reason
        /// </summary>
        [OperationContract]
        ReturnMessage RemoveAChild(RemoveChild Child, string UserID);

        [OperationContract]
        List<ListChild> GetChildren(string userid, int countryid, int locationid, int childnumber);

        [OperationContract]
        AdminChild GeteChild(string childid, string userid);

        [OperationContract]
        List<DownLoadFile> GetFile(DownLoadFile file);

        [OperationContract]
        List<ReturnMessage> SaveChild(AdminChild child, string userid);
        #endregion

        #region MarkLocation
        [OperationContract]
        ReturnMessage MarkLocationForUpdate(string UserID, string LocationID, DateTime VisitDate, int LeadDays, DateTime DueDate);

        [OperationContract]
        List<MarkLocationForUpdateValue> GetLocationForUpdate(string UserID, string LocationID, string countryID);

        [OperationContract]
        ReturnMessage CancelLocationForUpdate(string UserID, string LocationCalendarID);

        [OperationContract]
        ReturnMessage EditLocationForUpdate(string UserID, string LocationID, int LocationCalendarID, DateTime VisitDate, int LeadDays, DateTime DueDate);
        #endregion

        #region Enrollment
        /// <summary>
        /// Take the child data provided and runs the Enrollment process of the Business rules are met.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> EnrollAChild(string ChildID, string UserID);

        /// <summary>
        /// Saves the information in the provided child for future enfollment.
        /// </summary>
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare)]
        List<ReturnMessage> SaveEnrollment(EnrollChild Child, string UserID);

        /// <summary>
        /// Upload a supporting file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile(UploadFile File);

        /// <summary>
        /// Get the incomplete enrollments that have been saved by a user
        /// </summary>
        [OperationContract]
        List<ListChild> GetIncompleteEnrollments(string UserID, int CountryID, int LocationID, int ActionID);

        /// <summary>
        /// Get the saved child record for a incomplete enrollment
        /// </summary>
        [OperationContract]
        EnrollChild GetIncompleteChildEnrollment(string ChildID, string UserID);

        /// <summary>
        /// Get a enrollment file
        /// </summary>
        [OperationContract]
        List<DownLoadFile> GetEnrollmentFile(DownLoadFile photo);

        /// <summary>
        /// Delete Enrollment
        /// </summary>
        [OperationContract]
        List<ReturnMessage> DeleteEnrollment(string ChildID, string UserID);

        /// <summary>
        /// Delete a supporting file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> DeleteAnEnrollmentFile(UploadFile File);


        /*
        /// <summary>
        /// Upload a supporting Profile Image file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile_Profile_Image(string UserID, UploadFile File);


        /// <summary>
        /// Upload a supporting Consent file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile_Consent(string UserID, UploadFile File);

        /// <summary>
        /// Upload a supporting Action file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile_Action(string UserID, UploadFile File);

        /// <summary>
        /// Upload a supporting Art file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile_Art(string UserID, UploadFile File);

        /// <summary>
        /// Upload a supporting Letter file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnEnrollmentFile_Letter(string UserID, UploadFile File);
        */


        /// <summary>
        /// Upload a supporting Letter file for an Enrolled child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> Send_Content_File
            (
            string UserID, 
            UploadFile File, 
            Classes.Settingz.enum_Operation_Type e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type e_Content_Binary_Type
            );


        #endregion

        #region Update
        /// <summary>
        /// Take the child data provided and runs the Enrollment process of the Business rules are met.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> SubmitUpdate(string ChildID, string UserID);

        /// <summary>
        /// Saves the information in the provided for the Update.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> SaveUpdate(UpdateChild Child, string UserID);

        /// <summary>
        /// Upload a supporting file for an update child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UploadAnUpdateFile(UploadFile File);

        /// <summary>
        /// Get the incomplete enrollments that have been saved by a user
        /// </summary>
        [OperationContract]
        List<ListChild> GetIncompleteUpdates(string UserID, int CountryID, int LocationID, int ActionID);

        /// <summary>
        /// Get the saved child record for a incomplete update
        /// </summary>
        [OperationContract]
        List<AdminChild> GetIncompleteChildUpdate(string ChildID, string UserID);

        /// <summary>
        /// Delete Update record
        /// </summary>
        [OperationContract]
        List<ReturnMessage> DeleteUpdate(string ChildID, string UserID, int RemoveReasonID);

        /// <summary>
        /// Get the child record for an update
        /// </summary>
        [OperationContract]
        List<AdminChild> GetFieldAppUpdateChildren(string UserID, string LocationID);

        /// <summary>
        /// Get the saved child Profile photo
        /// </summary>
        [OperationContract]
        ChildProfilePhoto GetChildProfilePhoto(ChildProfilePhoto photo);

        /// <summary>
        /// Get a uploaded file
        /// </summary>
        [OperationContract]
        List<DownLoadFile> GetUpdateFile(DownLoadFile photo);

        /// <summary>
        /// Delete a supporting file for an update child.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> DeleteAnUpdateFile(UploadFile File);
        #endregion

        #region WorkFlow
        /// <summary>
        /// Get the open workflows
        /// </summary>
        [OperationContract]
        DataSet GetWorkFlows(int WorkFlowID, int CountryID, int LocationID, string UserID, string QCApprove);

        /// <summary>
        /// Get the workflow steps for a given instance
        /// </summary>
        [OperationContract]
        List<WorkFlowStep> GetChildWorkFlow(int PendingChildID, int WorkFlowID, string UserID, bool QCApprover);

        /// <summary>
        /// Save a workflow step
        /// </summary>
        [OperationContract]
        ReturnMessage SaveWorkFlow(WorkFlowStep WorkFlow, string UserID);

        /// <summary>
        /// Add a workflow step such as a decline
        /// </summary>
        [OperationContract]
        ReturnMessage AddSubWorkFlow(SubWorkFlow WorkFlow, string UserID);

        /// <summary>
        /// This is to decline a step for the provided reason. 
        /// </summary>
        [OperationContract]
        ReturnMessage DeclineStep(Decline Decline, string UserID);

        /// <summary>
        /// Update the information for a declined record.
        /// </summary>
        [OperationContract]
        List<ReturnMessage> UpdateDecline(AdminChild Child, string UserID);

        /// <summary>
        /// Get a list of translation workflows for the given country
        /// </summary>
        [OperationContract]
        DataSet GetTranslationWorkFlows(int ActionReason, int CountryID, int LocationID, int LanguageID, string UserID);

        /// <summary>
        /// Get the translations for a given child
        /// </summary>
        [OperationContract]
        List<TranslationStep> GetChildTranslations(string ChildID, int ChildNumber, string UserID);

        /// <summary>
        /// Get a list of declines that need to be worked
        /// </summary>
        [OperationContract]
        DataSet GetDeclineList(int WorkFlowID, int ChildNum, string ChildID, bool PendingOnly, string UserID, bool QCApprover, int CountryID, int LocationID);

        /// <summary>
        /// Get the declines for a given child
        /// </summary>
        [OperationContract]
        List<DeclineDetails> GetChildDeclines(string ChildID, int ChildNumber, bool PendingOnly, string UserID, bool QCApprover);

        /// <summary>
        /// Save a the translated mle
        /// </summary>
        [OperationContract]
        ReturnMessage SaveTranslatedMLE(string UserID, string ChildID, int LanguageCodeID, string MajorLifeDescription, int ChildMajorLifeEventID, int ChildMajorLifeEventLangID);
        #endregion

        #region ChildRecords
        [OperationContract]
        List<ReturnMessage> SaveChildRecord(AdminChild AdChild, string UserID);

        [OperationContract]
        List<ReturnMessage> UploadChildRecordFile(UploadFile File);

        [OperationContract]
        List<ListChild> GetChildRecords(string UserID, int CountryID, int LocationID/*, int ActionID*/);

        [OperationContract]
        List<DownLoadFile> GetChildRecordFile(DownLoadFile File);

        [OperationContract]
        List<Country_DropDown_Item> GetCountryRecords(string UserID);


        [OperationContract]
        List<DropDown_Item_w_ID> Get_Locations_For_User(string UserID, int Country_ID);


        #endregion



        [OperationContract]
        List<Decline_Reason_Codes> DeclineReasonCodes(string UserID);


        [OperationContract]
        List<Decline_Sub_Reason_Codes> DeclineSubReasonCodes(string UserID, int DeclineReasonCodeID);


    }
}
