﻿using PanMainAPI.Data;
using PanMainAPI.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PanMainAPI.DAO
{
    public class SPROCer
    {


        string _sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;


        public DataTable Get_SPROC_dt(string SPROC_name, string userid)
        {

            SqlConnection Con = new SqlConnection(_sqlConnection);

            DataTable the_datatable = new DataTable();

            try
            {

                SqlCommand cmd = new SqlCommand(SPROC_name, Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", userid);

                SqlDataAdapter adptr = new SqlDataAdapter();

                adptr.SelectCommand = cmd;

                Con.Open();

                adptr.Fill(the_datatable);

            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return the_datatable;

        }


        public DataTable Get_User_Assigned_Country_Details(string userid)
        {

            string SPROC_name = "usp_Get_User_Country";

            SqlConnection Con = new SqlConnection(_sqlConnection);

            DataTable the_datatable = new DataTable();

            try
            {

                SqlCommand cmd = new SqlCommand(SPROC_name, Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", userid);

                cmd.Parameters.AddWithValue("@Check_User_ID", userid);

                SqlDataAdapter adptr = new SqlDataAdapter();

                adptr.SelectCommand = cmd;

                Con.Open();

                adptr.Fill(the_datatable);

            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return the_datatable;

        }


        public List<Country_DropDown_Item> GetCountryRecords(string UserID)
        {

            string key_field = "Country_Code_ID";

            DataTable dt_Countries = Get_SPROC_dt("[Code].[usp_Get_Country_Codes]", UserID);

            dt_Countries.TableName = "Countries";

            dt_Countries.PrimaryKey = new DataColumn[] { dt_Countries.Columns[key_field] };

            string filter_is_active = "Active = 1";

            DataView dv_countries = new DataView(dt_Countries, filter_is_active, "", DataViewRowState.CurrentRows);

            DataTable filtered_countries = dv_countries.ToTable(dt_Countries.TableName);


            DataTable dt_Countries_For_A_Given_UserID = Get_User_Assigned_Country_Details(UserID);

            dt_Countries_For_A_Given_UserID.TableName = "UserCountries";


            string s_filter = "User_ID = '" + UserID + "'";


            List<Country_DropDown_Item> country_entries = new List<Country_DropDown_Item>();

            foreach (DataRow the_rec in filtered_countries.Rows)
            {
                int country_id = Convert.ToInt32(the_rec[key_field]);

                string the_filter = filter_is_active + " AND " + key_field + " = " + country_id.ToString();

                DataView dv = new DataView(dt_Countries_For_A_Given_UserID, the_filter, "", DataViewRowState.CurrentRows);

                if (dv.Count > 0)
                {

                    Country_DropDown_Item cdi = new Country_DropDown_Item();

                    cdi.Country_ID = Convert.ToInt32(the_rec[key_field]);

                    cdi.Country_Name = Convert.ToString(the_rec["Country_Name"]);

                    country_entries.Add(cdi);

                }

            }

            return country_entries;

        }


        public int? Get_Content_Type_Code_ID
            (
            string UserID,
            string Content_Type
            )
        {

            int? the_ID = null;

            string key_field = "Content_Type_Code_ID";

            DataTable dt_Content_Type_Codes = Get_SPROC_dt("[Code].[usp_Get_Content_Type_Codes]", UserID);

            dt_Content_Type_Codes.TableName = "Content_Type_Codes";

            dt_Content_Type_Codes.PrimaryKey = new DataColumn[] { dt_Content_Type_Codes.Columns[key_field] };

            string the_filter = "Content_Type = '" + Content_Type + "'";

            DataView dv_Content_Type_Code = new DataView(dt_Content_Type_Codes, the_filter, "", DataViewRowState.CurrentRows);

            if (dv_Content_Type_Code.Count == 1)
            {
                the_ID = (int)dv_Content_Type_Code[0][key_field];
            }

            return the_ID;

        }



        public List<DropDown_Item_w_ID> Get_Locations_For_User(string UserID, int Country_ID)
        {

            List<DropDown_Item_w_ID> l_records = new List<DropDown_Item_w_ID>();

            string key_field = "Country_Code_ID";

            DataTable dt_Countries = Get_SPROC_dt("[Code].[usp_Get_Country_Codes]", UserID);

            dt_Countries.TableName = "Countries";

            dt_Countries.PrimaryKey = new DataColumn[] { dt_Countries.Columns[key_field] };

            DataTable dt_Locations = Get_SPROC_dt("[Code].[usp_Get_Location_Codes]", UserID);

            dt_Locations.TableName = "Locations";

            dt_Locations.PrimaryKey = new DataColumn[] { dt_Locations.Columns["Location_Code_ID"] };

            string filter_is_active = "Active = 1";

            DataView dv_countries = new DataView
                (
                dt_Countries,
                filter_is_active,
                "",
                DataViewRowState.CurrentRows
                );

            DataTable filtered_countries = dv_countries.ToTable(dt_Countries.TableName);

            DataView dv_Locations = new DataView
                (
                dt_Locations,
                filter_is_active,
                "",
                DataViewRowState.CurrentRows
                );

            DataTable filtered_Locations = dv_Locations.ToTable(dt_Locations.TableName);

            DataTable dt_Countries_For_A_Given_UserID = Get_User_Assigned_Country_Details(UserID);

            dt_Countries_For_A_Given_UserID.TableName = "UserCountries";

            string s_filter = "User_ID = '" + UserID + "'";

            var qry = from locs in filtered_Locations.AsEnumerable()
                      join cntries in dt_Countries_For_A_Given_UserID.AsEnumerable()
                      on locs.Field<int>(key_field) equals cntries.Field<int>(key_field)
                      select new
                      {
                          Location_Code_ID = locs.Field<int>("Location_Code_ID"),
                          Location_Name = locs.Field<string>("Location_Name").Trim(),
                          Country_ID = locs.Field<int>(key_field)
                      };

            var the_locs =
                qry
                .Where
                (
                xx => xx.Country_ID == Country_ID
                )
                .OrderBy(zz => zz.Location_Name)
                .ToList()
                ;

            the_locs.ForEach
                (
                xx =>
                {

                    DropDown_Item_w_ID ddi = new DropDown_Item_w_ID();

                    ddi.DropDown_Caption = xx.Location_Name;

                    ddi.ID = xx.Location_Code_ID;

                    l_records.Add(ddi);

                }
                );

            return l_records;

        }


    }
}