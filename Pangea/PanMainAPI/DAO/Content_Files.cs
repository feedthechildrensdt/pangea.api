﻿using PanMainAPI.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PanMainAPI.DAO
{
    public class Content_Files
    {

        private static List<ReturnMessage> Upload_Content_File
            (
            string UserID,
            UploadFile file,
            string the_Content_Type,
            Classes.Settingz.enum_Operation_Type e_Operation_Type
            )
        {

            SPROCer sprocer = new SPROCer();

            int? the_Content_ID = sprocer.Get_Content_Type_Code_ID(UserID, the_Content_Type);

            file.ContentTypeID = the_Content_ID.Value;

            List<ReturnMessage> the_Result = null;

            switch (e_Operation_Type)
            {
                case Classes.Settingz.enum_Operation_Type.Enrollment:

                    Enrollment enroll = new Enrollment();

                    the_Result = enroll.UploadEnrollmentFile(file);

                    break;

                case Classes.Settingz.enum_Operation_Type.Update:

                    Update update = new Update();

                    the_Result = update.UploadUpdateFile(file);

                    break;
            }            

            return the_Result;

        }


        public static List<ReturnMessage> Process_Content_File_Upload
            (
            string UserID,
            UploadFile File,
            Settingz.enum_Operation_Type e_Operation_Type,
            Settingz.enum_Content_Binary_Type e_Content_Binary_Type
            )
        {
            string the_Content_File_Type = "";

            switch (e_Content_Binary_Type)
            {
                case Classes.Settingz.enum_Content_Binary_Type.Action:

                    the_Content_File_Type = "Action";

                    break;

                case Classes.Settingz.enum_Content_Binary_Type.Art:

                    the_Content_File_Type = "Art";

                    break;

                case Classes.Settingz.enum_Content_Binary_Type.Consent:

                    the_Content_File_Type = "Consent";

                    break;

                case Classes.Settingz.enum_Content_Binary_Type.Letter:

                    the_Content_File_Type = "Letter";

                    break;

                case Classes.Settingz.enum_Content_Binary_Type.Profile_Image:

                    the_Content_File_Type = "Profile";

                    break;

            }

            return Content_Files.Upload_Content_File
                        (
                        UserID,
                        File,
                        the_Content_File_Type,
                        e_Operation_Type
                        );
        }


        private static List<DownLoadFile> Get_DownloadFile_Given_Child_ID
            (
            DownLoadFile file,
            Guid Child_ID,
            Guid User_ID,
            Settingz.enum_Operation_Type e_Operation_Type
            )
        {
            List<DownLoadFile> files = new List<DownLoadFile>();

            SqlConnection Con = new SqlConnection(Settingz.sqlConnection);

            string the_Schema = "";

            switch (e_Operation_Type)
            {
                case Settingz.enum_Operation_Type.Enrollment:

                    the_Schema = "Enrollment";

                    break;

                case Settingz.enum_Operation_Type.Update:

                    the_Schema = "Pending";

                    break;
            }

            try
            {

                string the_SQL = "[" + the_Schema + "].[usp_Get_File]";

                SqlCommand cmnd = new SqlCommand(the_SQL, Con);

                cmnd.CommandType = CommandType.StoredProcedure;

                cmnd.Parameters.AddWithValue("@Child_id", Child_ID);

                cmnd.Parameters.AddWithValue("@User_ID", User_ID);

                Con.Open();

                SqlDataReader reader = cmnd.ExecuteReader();

                while (reader.Read())
                {
                    Add_Download_File
                        (
                        // file,
                        files,
                        reader,
                        Child_ID,
                        User_ID
                        );
                }
            }
            catch (Exception ex)
            {
                file.ChildID = "ERROR";

                file.UserID = "GetFile failed (2)";

                file.FileName = ex.Message;

                files.Add(file);
            }
            finally
            {
                Con.Close();
            }

            return files;
        }


        public static int ContentFile_Recs_Given_Child_ID_And_ContentTypeID_Count
            (
            DownLoadFile file,
            Guid Stream_ID,
            Guid User_ID,
            int the_ContentTypeID,
            Settingz.enum_Operation_Type e_Operation_Type
            )
        {

            List<DownLoadFile> the_file_recs =
                Get_DownloadFile_Given_Child_ID
                (
                    file,
                    Stream_ID,
                    User_ID,
                    e_Operation_Type
                    );

            int i_rows =
                the_file_recs
                .Where
                (
                    xx =>
                    xx.ContentTypeID == the_ContentTypeID
                    )
                .Count();

            return i_rows;
        }


        public static void Add_Download_File
            (
            List<DownLoadFile> files, 
            SqlDataReader reader,
            Guid Child_ID,
            Guid User_ID
            )
        {

            DownLoadFile fl = new DownLoadFile(Child_ID.ToString (), User_ID.ToString ());

            fl.FileName = reader["name"].ToString();

            fl.FileBytes = (byte[])reader["file_stream"];

            fl.ContentTypeID = (int)reader["Content_Type_ID"];

            fl.FileStreamID = reader["stream_id"].ToString();

            fl.PrimaryStreamID = reader["primary_stream_id"].ToString();

            files.Add(fl);
        }

    }


}