﻿using PanMainAPI.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PanMainAPI.DAO
{
    public class SystemFunctions
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public List<ReturnMessage> CheckForDuplicates(EnrollChild child, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataTable possibledup = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Get_Possible_Child_Duplicates]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                cmd.Parameters.AddWithValue("@First_Name", child.FirstName);
                cmd.Parameters.AddWithValue("@Middle_Name", child.MiddleName);
                cmd.Parameters.AddWithValue("@Last_Name", child.LastName);
                cmd.Parameters.AddWithValue("@Nickname", child.OtherNameGoesBy);
                cmd.Parameters.AddWithValue("@Date_Of_Birth", child.DateOfBirth);
                cmd.Parameters.AddWithValue("@Gender_Code_ID", child.GenderCodeID);
                cmd.Parameters.AddWithValue("@Location_Code_ID", child.LocationCodeID);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(possibledup);
            }
            catch (Exception ex)
            {
                ReturnMessage message = new ReturnMessage("CD01", "Errors", "Check Duplicate Error: " + ex.Message);
                returnMessages.Add(message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            if (possibledup.Rows.Count > 0)
            {
                foreach (DataRow row in possibledup.Rows)
                {
                    ReturnMessage message = new ReturnMessage(row["Child_ID"].ToString(), "Possible Duplicate", row["First_Name"].ToString() + " " + row["Middle_Name"].ToString() + " " + row["Last_Name"].ToString());
                    returnMessages.Add(message);
                }
            }

            return returnMessages;
        }

        public DataSet GetVersionDB(DBVersion currentdb)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_DB_Version]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", currentdb.UserID);
                cmd.Parameters.AddWithValue("@Major", currentdb.Major);
                cmd.Parameters.AddWithValue("@Minor", currentdb.Minor);
                cmd.Parameters.AddWithValue("@Build", currentdb.Build);
                cmd.Parameters.AddWithValue("@Revision", currentdb.Revision);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable("Errors");
                dt.Columns.Add("Message");
                dt.Rows.Add(dt.NewRow());
                dt.Rows[0][0] = ex.Message;
                DBVersion.Tables.Add(dt);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return DBVersion;
        }

        public List<DBConfigObject> GetDBConfig(string userid)
        {
            List<DBConfigObject> values = new List<DBConfigObject>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet dbConfig = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Config]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(dbConfig);


                foreach (DataRow row in dbConfig.Tables[0].Rows)
                {
                    DBConfigObject value = new DBConfigObject();
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Config_ID"].ToString()))
                        value.Config_ID = int.Parse(row["Config_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Name"].ToString()))
                        value.Name = row["Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Bit_Value"].ToString()))
                        value.Bit_Value = Boolean.Parse(row["Bit_Value"].ToString());
                    if (!string.IsNullOrEmpty(row["Datetime_Value"].ToString()))
                        value.Datetime_Value = DateTime.Parse(row["Datetime_Value"].ToString());
                    if (!string.IsNullOrEmpty(row["Dec_Value"].ToString()))
                        value.Dec_Value = Decimal.Parse(row["Dec_Value"].ToString());
                    if (!string.IsNullOrEmpty(row["Int_Value"].ToString()))
                        value.Int_Value = int.Parse(row["Int_Value"].ToString());
                    if (!string.IsNullOrEmpty(row["Text_Value"].ToString()))
                        value.Text_Value = row["Text_Value"].ToString();

                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                DBConfigObject value = new DBConfigObject();
                value.Config_ID = -1;
                value.Name = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }
    }
}