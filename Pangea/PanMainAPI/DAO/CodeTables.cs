﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using PanMainAPI.Data;

using System.Linq;
using System.IO;
using Newtonsoft.Json;

namespace PanMainAPI.DAO
{
    public class CodeTables
    {
        static string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public DataSet GetCodeTables(string userid)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Codes_Tables]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable("Errors");
                dt.Columns.Add("Message");
                dt.Rows.Add(dt.NewRow());
                dt.Rows[0][0] = ex.Message;
                codetables.Tables.Add(dt);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return codetables;
        }

        public ReturnMessage Child_Remove_Reason(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Child_Remove_Reason", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Child_Remove_Reason]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_Remove_Reason", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Gender(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Gender", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Gender]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Gender", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Gender_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Lives_With(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Lives_With", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Lives_With]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Lives_With", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Lives_With_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Personality_Type(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Personality_Type", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Personality_Type]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Personality_Type", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Personality_Type_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Chore(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Chore", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Chore]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Chore", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Chore_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Favorite_Activity(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Favorite_Activity", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Favorite_Activity]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Favorite_Activity", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Favorite_Activity_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Favorite_Learning(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Favorite_Learning", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Favorite_Learning]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Favorite_Learning", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Favorite_Learning_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Grade_Level(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Grade_Level", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Grade_Level]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Grade_Level", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Grade_Level_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Content_Type(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Content_Type", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Content_Type]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Content_Type", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Description", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Public_Description", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Required", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Content_Type_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Country(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Country", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Country]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Country_Code", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Country_Name", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Region_Code_ID", CodeValue.ParentCodeID);
                cmd.Parameters.AddWithValue("@Default_Language", CodeValue.PublicDescription);
                cmd.Parameters.AddWithValue("@Min_School_Age", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }


        public ReturnMessage Location(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Location", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Location]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Location_Name", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Sponsorship_Site_Code", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Sponsorship_Site", CodeValue.SponsorshipSite);
                cmd.Parameters.AddWithValue("@Country_Code_ID", CodeValue.ParentCodeID);
                cmd.Parameters.AddWithValue("@Default_Language", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public ReturnMessage Region(CodeTableValue CodeValue, string userid)
        {
            ReturnMessage msg = new ReturnMessage("0", "Region", "Nothing was processed");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Region]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Region_Code", CodeValue.Code);
                cmd.Parameters.AddWithValue("@Region_Name", CodeValue.Description);
                cmd.Parameters.AddWithValue("@Default_Language", CodeValue.MinAge);
                if (CodeValue.CodeID > 0)
                    cmd.Parameters.AddWithValue("@Region_Code_ID", CodeValue.CodeID);
                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return msg;
        }

        public List<CodeTableValue> Get_Child_Remove_Reason(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Child_Remove_Reason_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Child_Remove_Reason");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Child_Remove_Reason_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Child_Remove_Reason_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Child_Remove_Reason"].ToString()))
                        value.Code = row["Child_Remove_Reason"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Child_Remove_Reason");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Gender(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Gender_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Gender");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Gender_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Gender_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Gender"].ToString()))
                        value.Code = row["Gender"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Gender");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Lives_With(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Lives_With_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Lives_With");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Lives_With_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Lives_With_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Lives_With"].ToString()))
                        value.Code = row["Lives_With"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Lives_With");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Personality_Type(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Personality_Type_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Personality_Type");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Personality_Type_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Personality_Type_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Personality_Type"].ToString()))
                        value.Code = row["Personality_Type"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Personality_Type");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Chore(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Chore_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Chore");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Chore_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Chore_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Chore"].ToString()))
                        value.Code = row["Chore"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Min_Age"].ToString()))
                        value.MinAge = int.Parse(row["Min_Age"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Chore");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Favorite_Activity(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Favorite_Activity_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Favorite_Activity");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Favorite_Activity_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Favorite_Activity_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Favorite_Activity"].ToString()))
                        value.Code = row["Favorite_Activity"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Min_Age"].ToString()))
                        value.MinAge = int.Parse(row["Min_Age"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Favorite_Activity");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Favorite_Learning(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Favorite_Learning_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Favorite_Learning");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Favorite_Learning_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Favorite_Learning_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Favorite_Learning"].ToString()))
                        value.Code = row["Favorite_Learning"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Min_Age"].ToString()))
                        value.MinAge = int.Parse(row["Min_Age"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Favorite_Learning");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Grade_Level(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Grade_Level_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Grade_Level");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Grade_Level_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Grade_Level_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Grade_Level"].ToString()))
                        value.Code = row["Grade_Level"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Min_Age"].ToString()))
                        value.MinAge = int.Parse(row["Min_Age"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Grade_Level");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Content_Type(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Content_Type_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Content_Type");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Content_Type_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Content_Type_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Content_Type"].ToString()))
                        value.Code = row["Content_Type"].ToString();
                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.Description = row["Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Public_Description"].ToString()))
                        value.PublicDescription = row["Public_Description"].ToString();
                    if (!string.IsNullOrEmpty(row["Required"].ToString()))
                        if (bool.Parse(row["Required"].ToString()))
                            value.MinAge = 1;
                        else
                            value.MinAge = 0;
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Content_Type");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Country(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Country_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Country");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Country_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Country_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Country_Code"].ToString()))
                        value.Code = row["Country_Code"].ToString();
                    if (!string.IsNullOrEmpty(row["Country_Name"].ToString()))
                        value.Description = row["Country_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Default_Language"].ToString()))
                        value.PublicDescription = row["Default_Language"].ToString();
                    if (!string.IsNullOrEmpty(row["Min_School_Age"].ToString()))
                        value.MinAge = int.Parse(row["Min_School_Age"].ToString());
                    if (!string.IsNullOrEmpty(row["Region_Code_ID"].ToString()))
                        value.ParentCodeID = int.Parse(row["Region_Code_ID"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Country");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Location(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Location_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Location");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Location_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Location_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Location_Name"].ToString()))
                        value.Code = row["Location_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Sponsorship_Site"].ToString()))
                        value.Description = row["Sponsorship_Site"].ToString();
                    if (!string.IsNullOrEmpty(row["Default_Language"].ToString()))
                        value.MinAge = int.Parse(row["Default_Language"].ToString());
                    if (!string.IsNullOrEmpty(row["Country_Code_ID"].ToString()))
                        value.ParentCodeID = int.Parse(row["Country_Code_ID"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Location");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }

        public List<CodeTableValue> Get_Region(string userid)
        {
            List<CodeTableValue> values = new List<CodeTableValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet codetables = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Region_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(codetables);
                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Region");
                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());
                    if (!string.IsNullOrEmpty(row["Region_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Region_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Region_Code"].ToString()))
                        value.Code = row["Region_Code"].ToString();
                    if (!string.IsNullOrEmpty(row["Region_Name"].ToString()))
                        value.Description = row["Region_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Default_Language"].ToString()))
                        value.MinAge = int.Parse(row["Default_Language"].ToString());
                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("Region");
                value.CodeID = -1;
                value.Code = "An Error has occured.";
                value.Description = ex.Message;
                values.Add(value);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return values;
        }


        public static List<Decline_Reason_Codes> GetDeclineReasonCodes(string UserID)
        {

            List<Decline_Reason_Codes> DeclineReasonCodes = new List<Decline_Reason_Codes>();

            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {

                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Decline_Reason_Codes]", Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", UserID);

                Con.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    Decline_Reason_Codes drc = new Decline_Reason_Codes();

                    drc.Active = (bool)reader["Active"];

                    drc.Action_ID = (int)reader["Action_ID"];

                    drc.Action_User = (Guid)reader["Action_User"];

                    drc.Action_Date = (DateTime)reader["Action_Date"];

                    Type the_type = reader["Action_Reason"].GetType();

                    if(the_type.Name != "DBNull")
                    {
                        drc.Action_Reason = (int?)reader["Action_Reason"];
                    }
                    {
                        drc.Action_Reason = null;
                    }

                    drc.Decline_Reason = reader["Decline_Reason"].ToString();

                    drc.Description = reader["Description"].ToString();

                    drc.Decline_Reason_Code_ID = (int)reader["Decline_Reason_Code_ID"];

                    DeclineReasonCodes.Add(drc);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return DeclineReasonCodes;

        }



        public static List<Decline_Sub_Reason_Codes> GetDeclineSubReasonCodes
            (
            string UserID, 
            int? DeclineReasonCodeID
            )
        {

            bool Filter_For_Specific_Decline_Reason_Code = DeclineReasonCodeID.HasValue;

            List<Decline_Sub_Reason_Codes> DeclineSubReasonCodes = new List<Decline_Sub_Reason_Codes>();

            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {

                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Decline_SubReason_Codes]", Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", UserID);

                if (Filter_For_Specific_Decline_Reason_Code)
                {
                    cmd.Parameters.AddWithValue("@Decline_Reason_Code_ID", DeclineReasonCodeID.Value);
                }

                Con.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    Decline_Sub_Reason_Codes dsrc = new Decline_Sub_Reason_Codes();

                    dsrc.Active = (bool)reader["Active"];

                    dsrc.Action_ID = (int)reader["Action_ID"];

                    dsrc.Action_User = (Guid)reader["Action_User"];

                    dsrc.Action_Date = (DateTime)reader["Action_Date"];

                    Type the_type = reader["Action_Reason"].GetType();

                    if (the_type.Name != "DBNull")
                    {
                        dsrc.Action_Reason = (int?)reader["Action_Reason"];
                    }
                    {
                        dsrc.Action_Reason = null;
                    }

                    dsrc.Decline_SubReason_Code_ID = (int)reader["Decline_SubReason_Code_ID"];

                    dsrc.Decline_Reason_Code_ID = (int)reader["Decline_Reason_Code_ID"];

                    dsrc.Decline_SubReason = reader["Decline_SubReason"].ToString();

                    dsrc.Description = reader["Description"].ToString();

                    DeclineSubReasonCodes.Add(dsrc);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return DeclineSubReasonCodes;

        }


        public ReturnMessage DeclineSubReason_Editor(CodeTableValue CodeValue, string userid)
        {
                
            ReturnMessage msg = new ReturnMessage("0", "Decline Reason", "Nothing processed");

            SqlConnection Con = new SqlConnection(sqlConnection);

            DataSet DBVersion = new DataSet();

            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Add_Decline_SubReason]", Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", userid);

                cmd.Parameters.AddWithValue("@Active", CodeValue.Active);


                if (CodeValue.CodeID == 0)
                {
                    cmd.Parameters.AddWithValue("@Description", CodeValue.Description);

                    cmd.Parameters.AddWithValue("@Decline_SubReason", CodeValue.Description);

                    cmd.Parameters.AddWithValue("@Decline_Reason_Code_ID", CodeValue.ParentCodeID);
                }
                else
                {
                    Decline_Sub_Reason_Codes the_DeclineReasonCodes =
                        CodeTables
                        .GetDeclineSubReasonCodes(userid, null)
                        .Where(xx => xx.Decline_SubReason_Code_ID == CodeValue.CodeID)
                        .First();

                    string the_Edit_Decline_SubReason = the_DeclineReasonCodes.Decline_SubReason;                

                    int the_Decline_Reason_Code_ID = the_DeclineReasonCodes.Decline_Reason_Code_ID;

                    cmd.Parameters.AddWithValue("@Decline_SubReason", the_Edit_Decline_SubReason);

                    cmd.Parameters.AddWithValue("@Description", CodeValue.PublicDescription);

                    cmd.Parameters.AddWithValue("@Decline_SubReason_Code_ID", CodeValue.CodeID);

                    cmd.Parameters.AddWithValue("@Decline_Reason_Code_ID", the_Decline_Reason_Code_ID);
                }

                SqlDataAdapter adptr = new SqlDataAdapter();

                adptr.SelectCommand = cmd;

                Con.Open();

                adptr.Fill(DBVersion);

                msg = new ReturnMessage("1", "Success");
            }
            catch (Exception ex)
            {
                msg = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return msg;

        }


        public List<CodeTableValue> Get_DeclineSubReasons(string userid)
        {

            List<Data.Decline_Reason_Codes> l_Decline_Reason_Codes_Get = CodeTables.GetDeclineReasonCodes(userid);

            List<CodeTableValue> values = new List<CodeTableValue>();

            SqlConnection Con = new SqlConnection(sqlConnection);

            DataSet codetables = new DataSet();

            try
            {
                SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Decline_SubReason_Codes]", Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", userid);

                SqlDataAdapter adptr = new SqlDataAdapter();

                adptr.SelectCommand = cmd;

                Con.Open();

                adptr.Fill(codetables);

                foreach (DataRow row in codetables.Tables[0].Rows)
                {
                    CodeTableValue value = new CodeTableValue("Region");

                    if (!string.IsNullOrEmpty(row["Active"].ToString()))
                        value.Active = bool.Parse(row["Active"].ToString());

                    int Decline_Reason_Code_ID = Convert.ToInt32(row["Decline_Reason_Code_ID"]);

                    string the_reason_Code =
                        l_Decline_Reason_Codes_Get
                        .Where
                        (
                            xx =>
                            xx.Decline_Reason_Code_ID == Decline_Reason_Code_ID
                            )
                        .FirstOrDefault()
                        .Decline_Reason;

                    if (!string.IsNullOrEmpty(row["Decline_SubReason_Code_ID"].ToString()))
                        value.CodeID = int.Parse(row["Decline_SubReason_Code_ID"].ToString());

                    value.Code = the_reason_Code;  

                    if (!string.IsNullOrEmpty(row["Decline_SubReason"].ToString()))
                        value.Description = row["Decline_SubReason"].ToString();

                    if (!string.IsNullOrEmpty(row["Description"].ToString()))
                        value.PublicDescription = row["Description"].ToString();

                    value.MinAge = -100; 

                    values.Add(value);
                }
            }
            catch (Exception ex)
            {
                CodeTableValue value = new CodeTableValue("DeclineSubReasons");

                value.CodeID = -1;

                value.Code = "An Error has occured.";

                value.Description = ex.Message;

                values.Add(value);
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return values;

        }


    }


}