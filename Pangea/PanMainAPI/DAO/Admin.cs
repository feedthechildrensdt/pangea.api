﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PanMainAPI.DAO
{
    public class Admin
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public ReturnMessage RemoveChild(RemoveChild child, string userid)
        {
            ReturnMessage ret = new ReturnMessage("0", "Success");
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet DBVersion = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Removal]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", child.RemovalReasonID);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(DBVersion);
            }
            catch (Exception ex)
            {
                ret = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return ret;
        }

        public List<ListChild> GetChildren(string userid, int countryid, int locationid, int childnum)
        {
            List<ListChild> children = new List<ListChild>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (countryid > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", countryid);
                if (locationid > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                if (childnum > 0)
                    cmd.Parameters.AddWithValue("@Child_Number", childnum);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrEmpty(reader["Date_of_Birth"].ToString()))
                        children.Add(new ListChild(reader["Child_ID"].ToString(), reader["Child_Number"].ToString(), reader["Location_Name"].ToString(), reader["First_Name"].ToString(), reader["Middle_Name"].ToString(), reader["Last_Name"].ToString(), reader["Nickname"].ToString(), DateTime.Parse(reader["Date_of_Birth"].ToString()), reader["Gender"].ToString()));
                }
            }
            catch (Exception ex)
            {
                ListChild error = new ListChild();
                error.FirstName = "ERROR";
                error.LastName = ex.Message;
                error.MiddleName = "1";
                children.Add(error);
            }
            finally
            {
                Con.Close();
            }

            return children;
        }

        public AdminChild GeteChild(string childid, string userid)
        {
            AdminChild child = new AdminChild();
            child.ChildID = childid;

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_A_Saved_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrEmpty(reader["Child_Number"].ToString()))
                        child.ChildNum = reader["Child_Number"].ToString();
                    if (!string.IsNullOrEmpty(reader["First_Name"].ToString()))
                        child.FirstName = reader["First_Name"].ToString();
                    if (!string.IsNullOrEmpty(reader["Middle_Name"].ToString()))
                        child.MiddleName = reader["Middle_Name"].ToString();
                    if (!string.IsNullOrEmpty(reader["Last_Name"].ToString()))
                        child.LastName = reader["Last_Name"].ToString();
                    if (!string.IsNullOrEmpty(reader["Date_of_Birth"].ToString()))
                        child.DateOfBirth = DateTime.Parse(reader["Date_of_Birth"].ToString());
                    if (!string.IsNullOrEmpty(reader["Grade_Level_Code_ID"].ToString()))
                        child.GradeLevelCodeID = int.Parse(reader["Grade_Level_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Health_Status_Code_ID"].ToString()))
                        child.HealthStatusCodeID = int.Parse(reader["Health_Status_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Lives_With_Code_ID"].ToString()))
                        child.LivesWithCodeID = int.Parse(reader["Lives_With_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Favorite_Learning_Code_ID"].ToString()))
                        child.FavoriteLearningCodeID = int.Parse(reader["Favorite_Learning_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Gender_Code_ID"].ToString()))
                        child.GenderCodeID = int.Parse(reader["Gender_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Number_Brothers"].ToString()))
                        child.NumberOfBrothers = int.Parse(reader["Number_Brothers"].ToString());
                    if (!string.IsNullOrEmpty(reader["Number_Sisters"].ToString()))
                        child.NumberOfSisters = int.Parse(reader["Number_Sisters"].ToString());
                    if (!string.IsNullOrEmpty(reader["Disability_Status"].ToString()))
                        child.DisabilityStatus = bool.Parse(reader["Disability_Status"].ToString());
                    if (!string.IsNullOrEmpty(reader["Location_Code_ID"].ToString()))
                        child.LocationCodeID = int.Parse(reader["Location_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Nickname"].ToString()))
                        child.OtherNameGoesBy = reader["Nickname"].ToString();
                }
                Con.Close();

                try
                {
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_Personality_Type]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.PersonalityTypeID = int.Parse(subreader["Personality_Type_Code_ID"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GetUpdate failed (2)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }

                try
                {
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_Favorite_Activities]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.FavoriteActivitieIDs.Add(int.Parse(subreader["Favorite_Activity_Code_ID"].ToString()));
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GeteUpdate failed (3)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }


                try
                {
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_Chores]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.ChoreIDs.Add(int.Parse(subreader["Chore_Code_ID"].ToString()));
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GeteUpdate failed (4)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }

                try
                {
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_Major_Life_Event]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.MajorLifeEvent = subreader["Description"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GeteUpdate failed (5)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
                child.FirstName = "ERROR";
                child.MiddleName = "GeteUpdate failed (1)";
                child.LastName = ex.Message;
            }
            return child;
        }

        public List<DownLoadFile> GetFile(DownLoadFile file)
        {
            List<DownLoadFile> files = new List<DownLoadFile>();
            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_File]", Con);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.AddWithValue("@User_ID", file.UserID);
                if (!string.IsNullOrEmpty(file.ChildID))
                    cmnd.Parameters.AddWithValue("@Child_ID", file.ChildID);
                if (file.FileTypeID > 0)
                    cmnd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);
                if (file.ContentTypeID > 0)
                    cmnd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);
                if (file.FileID > 0)
                    cmnd.Parameters.AddWithValue("@File_ID", file.FileID);
                //else
                //    cmnd.Parameters.AddWithValue("@Current", 1);
                Con.Open();
                SqlDataReader reader = cmnd.ExecuteReader();
                while (reader.Read())
                {
                    DownLoadFile fl = new DownLoadFile(file.ChildID, file.UserID);
                    fl.FileName = reader["name"].ToString();
                    fl.FileBytes = (byte[])reader["file_stream"];
                    fl.ContentTypeID = (int)reader["Content_Type_ID"];
                    fl.FileStreamID = reader["stream_id"].ToString();
                    fl.PrimaryStreamID = reader["primary_stream_id"].ToString();
                    files.Add(fl);
                }
            }
            catch (Exception ex)
            {
                file.ChildID = "ERROR";
                file.UserID = "GetFile failed (2)";
                file.FileName = ex.Message;
                files.Add(file);
            }
            finally
            {
                Con.Close();
            }

            return files;
        }

        public List<ReturnMessage> SaveChild(AdminChild child, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            //Add db connection and procesing here. 
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Add_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                cmd.Parameters.AddWithValue("@First_Name", child.FirstName);
                cmd.Parameters.AddWithValue("@Last_Name", child.LastName);
                cmd.Parameters.AddWithValue("@Middle_Name", child.MiddleName);
                cmd.Parameters.AddWithValue("@Date_of_Birth", child.DateOfBirth);
                cmd.Parameters.AddWithValue("@Grade_Level_Code_ID", child.GradeLevelCodeID);
                cmd.Parameters.AddWithValue("@Health_Status_Code_ID", child.HealthStatusCodeID);
                cmd.Parameters.AddWithValue("@Lives_With_Code_ID", child.LivesWithCodeID);
                cmd.Parameters.AddWithValue("@Favorite_Learning_Code_ID", child.FavoriteLearningCodeID);
                //cmd.Parameters.AddWithValue("@Child_Record_Status_Code_ID", );
                //cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", );
                cmd.Parameters.AddWithValue("@Gender_Code_ID", child.GenderCodeID);
                cmd.Parameters.AddWithValue("@Number_Brothers", child.NumberOfBrothers);
                cmd.Parameters.AddWithValue("@Number_Sisters", child.NumberOfSisters);
                cmd.Parameters.AddWithValue("@Disability_Status", child.DisabilityStatus);
                cmd.Parameters.AddWithValue("@Location_Code_ID", child.LocationCodeID);
                cmd.Parameters.AddWithValue("@NickName", child.OtherNameGoesBy);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(child.ChildID))
                        child.ChildID = reader["Child_ID"].ToString();
                    returnMessages.Add(new ReturnMessage(reader["Child_ID"].ToString(), "Child_ID", "Success! Child details save completed. All current information has been saved."));
                }
                Con.Close();

                try
                {
                    // Clear is called so that only the current codes are attached to the child
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Clear_Child_Codes]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    cmnd.Parameters.AddWithValue("@End_Date", DateTime.Now);
                    Con.Open();
                    cmnd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    returnMessages.Add(new ReturnMessage("-3", "ERROR", "UpdateSave failed: " + ex.Message));
                }
                finally
                {
                    Con.Close();
                }

                if (child.PersonalityTypeID != 0)
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Personality_Type]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Personality_Type_Code_ID", child.PersonalityTypeID);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-4", "ERROR", "UpdateSave failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }

                if (child.FavoriteActivitieIDs != null)
                    if (child.FavoriteActivitieIDs.Count > 0)
                        foreach (int ActivityID in child.FavoriteActivitieIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Favorite_Activity]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Favorite_Activity_Code_ID", ActivityID);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-5", "ERROR", "UpdateSave failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }

                if (child.ChoreIDs != null)
                    if (child.ChoreIDs.Count > 0)
                    {
                        foreach (int Chore in child.ChoreIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Chore]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Chore_Code_ID", Chore);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-6", "ERROR", "UpdateSave failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }

                if (child.ConfirmedNonDups != null)
                    if (child.ConfirmedNonDups.Count > 0)
                    {
                        foreach (string childid in child.ConfirmedNonDups)
                        {
                            // call db add not dup
                        }
                    }

                if (!string.IsNullOrEmpty(child.MajorLifeEvent))
                {
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Major_Life_Event]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Language_Code_ID", child.LanguageCodeID);
                        cmnd.Parameters.AddWithValue("@Major_Life_Description", child.MajorLifeEvent);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-7", "ERROR", "UpdateSave failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "updateSave failed: " + ex.Message));
            }

            // save non dup list

            return returnMessages;
        }
    }
}