﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PanMainAPI.DAO
{
    public class ChildRecord
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public List<ReturnMessage> SaveChildRecord(AdminChild child, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            //Add db connection and procesing here. 
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Add_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                cmd.Parameters.AddWithValue("@Child_Number", child.ChildNum);
                cmd.Parameters.AddWithValue("@First_Name", child.FirstName);
                cmd.Parameters.AddWithValue("@Last_Name", child.LastName);
                cmd.Parameters.AddWithValue("@Middle_Name", child.MiddleName);
                cmd.Parameters.AddWithValue("@Date_of_Birth", child.DateOfBirth);
                cmd.Parameters.AddWithValue("@Grade_Level_Code_ID", child.GradeLevelCodeID);
                cmd.Parameters.AddWithValue("@Health_Status_Code_ID", child.HealthStatusCodeID);
                cmd.Parameters.AddWithValue("@Lives_With_Code_ID", child.LivesWithCodeID);
                cmd.Parameters.AddWithValue("@Favorite_Learning_Code_ID", child.FavoriteLearningCodeID);
                //cmd.Parameters.AddWithValue("@Child_Record_Status_Code_ID", );
                //cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", );
                cmd.Parameters.AddWithValue("@Gender_Code_ID", child.GenderCodeID);
                cmd.Parameters.AddWithValue("@Number_Brothers", child.NumberOfBrothers);
                cmd.Parameters.AddWithValue("@Number_Sisters", child.NumberOfSisters);
                cmd.Parameters.AddWithValue("@Disability_Status", child.DisabilityStatus);
                cmd.Parameters.AddWithValue("@Location_Code_ID", child.LocationCodeID);
                cmd.Parameters.AddWithValue("@NickName", child.OtherNameGoesBy);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(child.ChildID))
                        child.ChildID = reader["Child_ID"].ToString();
                    returnMessages.Add(new ReturnMessage(reader["Child_ID"].ToString(), "Child_ID", "Success! Child Record save completed."));
                }
                Con.Close();

                try
                {
                    // Clear is called so that only the current codes are attached to the child
                    SqlCommand cmnd = new SqlCommand("[dbo].[usp_Clear_Child_Codes]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    cmnd.Parameters.AddWithValue("@End_Date", DateTime.Now);
                    Con.Open();
                    cmnd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    returnMessages.Add(new ReturnMessage("-3", "ERROR", "SaveTheChild failed: " + ex.Message));
                }
                finally
                {
                    Con.Close();
                }

                if (child.PersonalityTypeID != 0)
                {
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Personality_Type]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Personality_Type_Code_ID", child.PersonalityTypeID);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-4", "ERROR", "SaveTheChild failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }
                }

                if (child.FavoriteActivitieIDs != null)
                {
                    if (child.FavoriteActivitieIDs.Count > 0)
                    {
                        foreach (int ActivityID in child.FavoriteActivitieIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Favorite_Activity]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Favorite_Activity_Code_ID", ActivityID);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-5", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }
                }

                if (child.ChoreIDs != null)
                {
                    if (child.ChoreIDs.Count > 0)
                    {
                        foreach (int Chore in child.ChoreIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Chore]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Chore_Code_ID", Chore);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-6", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }
                }

                if (child.ConfirmedNonDups != null)
                {
                    if (child.ConfirmedNonDups.Count > 0)
                    {
                        foreach (string childid in child.ConfirmedNonDups)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Non_Duplicates]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID_1", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Child_ID_2", childid);
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-7", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(child.MajorLifeEvent))
                {
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[dbo].[usp_Add_Child_Major_Life_Event]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Language_Code_ID", child.LanguageCodeID);
                        cmnd.Parameters.AddWithValue("@Major_Life_Description", child.MajorLifeEvent);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-7", "ERROR", "UpdateSave failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "SaveTheChild failed: " + ex.Message));
            }

            // save non dup list

            return returnMessages;
        }

        public List<ReturnMessage> UploadChildRecordFile(UploadFile file)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Add_Child_File]", Con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", file.UserID);
                cmd.Parameters.AddWithValue("@Child_ID", file.ChildID);
                cmd.Parameters.AddWithValue("@File", file.FileBytes);
                cmd.Parameters.AddWithValue("@FileName", file.FileName);
                cmd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);
                cmd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);
                if (file.MetaData != null && file.MetaData != new FileMetaData())
                {
                    cmd.Parameters.AddWithValue("@Make", file.MetaData.Make);
                    cmd.Parameters.AddWithValue("@Model", file.MetaData.Model);
                    cmd.Parameters.AddWithValue("@Software", file.MetaData.Software);
                    cmd.Parameters.AddWithValue("@DateTime", file.MetaData.DateTaken);
                    cmd.Parameters.AddWithValue("@DateTimeOriginal", file.MetaData.Original);
                    cmd.Parameters.AddWithValue("@DateTimeDigitized", file.MetaData.Digitized);
                    cmd.Parameters.AddWithValue("@GPSVersionID", file.MetaData.GPSVersionID);
                    cmd.Parameters.AddWithValue("@GPSLatitudeRef", file.MetaData.GPSLatitudeRef);
                    cmd.Parameters.AddWithValue("@GPSLatitude", file.MetaData.GPSLatitude);
                    cmd.Parameters.AddWithValue("@GPSLongitudeRef", file.MetaData.GPSLongitudeRef);
                    cmd.Parameters.AddWithValue("@GPSLongitude", file.MetaData.GPSLongitude);
                    cmd.Parameters.AddWithValue("@GPSAltitudeRef", file.MetaData.GPSAltitudeRef);
                    cmd.Parameters.AddWithValue("@GPSAltitude", file.MetaData.GPSAltitude);
                    cmd.Parameters.AddWithValue("@GPSTimeStamp", file.MetaData.GPSTimeStamp);
                    cmd.Parameters.AddWithValue("@GPSImgDirectionRef", file.MetaData.GPSImgDirectionRef);
                    cmd.Parameters.AddWithValue("@GPSDateStamp", file.MetaData.GPSDateStamp);
                }
                Con.Open();
                cmd.ExecuteNonQuery();
                returnMessages.Add(new ReturnMessage("0", "Success", "Success! File has successfully been saved."));
            }
            catch (Exception ex)
            {
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "UploadFile failed: " + ex.Message));
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return returnMessages;
        }

        public List<ListChild> GetChildRecords(string userid, int countryid, int locationid/*, int actionid*/)
        {
            List<ListChild> children = new List<ListChild>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (countryid > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", countryid);
                if (locationid > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    children.Add(new ListChild(reader["Child_ID"].ToString(), reader["Child_Number"].ToString(), reader["Location_Name"].ToString(), reader["First_Name"].ToString(), reader["Middle_Name"].ToString(), reader["Last_Name"].ToString(), reader["Nickname"].ToString(), DateTime.Parse(reader["Date_of_Birth"].ToString()), reader["Gender"].ToString()));
                }
            }
            catch (Exception ex)
            {
                ListChild error = new ListChild();
                error.FirstName = "ERROR";
                error.ChildID = ex.Message;
                children.Add(error);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return children;
        }

        public List<DownLoadFile> GetChildRecordFile(DownLoadFile file)
        {
            List<DownLoadFile> files = new List<DownLoadFile>();
            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmnd = new SqlCommand("[dbo].[usp_Get_File]", Con);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.AddWithValue("@User_ID", file.UserID);
                if (!string.IsNullOrEmpty(file.ChildID))
                    cmnd.Parameters.AddWithValue("@Child_ID", file.ChildID);
                if (file.FileTypeID > 0)
                    cmnd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);
                if (file.ContentTypeID > 0)
                    cmnd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);
                if (file.FileID > 0)
                    cmnd.Parameters.AddWithValue("@File_ID", file.FileID);
                else
                    cmnd.Parameters.AddWithValue("@Current", 1);
                Con.Open();
                SqlDataReader reader = cmnd.ExecuteReader();
                while (reader.Read())
                {
                    file.FileName = reader["name"].ToString();
                    file.FileBytes = (byte[])reader["file_stream"];
                    file.ContentTypeID = (int)reader["Content_Type_ID"];
                    files.Add(file);
                }
            }
            catch (Exception ex)
            {
                file.ChildID = "ERROR";
                file.UserID = "GetFile failed (2)";
                file.FileName = ex.Message;
                files.Add(file);
            }
            finally
            {
                Con.Close();
            }

            return files;
        }
    }
}