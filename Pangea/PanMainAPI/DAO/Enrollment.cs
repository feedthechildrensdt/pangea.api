﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PanMainAPI.DAO
{
    public class Enrollment
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public List<ReturnMessage> EnrollTheChild(string childid, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();
            DataTable Results = new DataTable();


            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Enroll_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", childid);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(Results);


                returnMessages.Add(new ReturnMessage(Results.Rows[0]["Child_Number"].ToString(), "Child_Number", "Success! Child has been sent for enrollment successfully."));

            }
            catch (Exception ex)
            {
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "EnrollTheChild failed: " + ex.Message));
            }
            finally
            {
                Con.Close();
            }

            return returnMessages;
        }

        public List<ReturnMessage> SaveTheChild(EnrollChild child, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            //Add db connection and procesing here. 
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Add_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (!String.IsNullOrEmpty(child.ChildID))
                    cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                cmd.Parameters.AddWithValue("@First_Name", child.FirstName);
                cmd.Parameters.AddWithValue("@Last_Name", child.LastName);
                cmd.Parameters.AddWithValue("@Middle_Name", child.MiddleName);
                cmd.Parameters.AddWithValue("@Date_of_Birth", child.DateOfBirth);
                cmd.Parameters.AddWithValue("@Grade_Level_Code_ID", child.GradeLevelCodeID);
                cmd.Parameters.AddWithValue("@Health_Status_Code_ID", child.HealthStatusCodeID);
                cmd.Parameters.AddWithValue("@Lives_With_Code_ID", child.LivesWithCodeID);
                cmd.Parameters.AddWithValue("@Favorite_Learning_Code_ID", child.FavoriteLearningCodeID);
                //cmd.Parameters.AddWithValue("@Child_Record_Status_Code_ID", );
                //cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", );
                cmd.Parameters.AddWithValue("@Gender_Code_ID", child.GenderCodeID);
                cmd.Parameters.AddWithValue("@Number_Brothers", child.NumberOfBrothers);
                cmd.Parameters.AddWithValue("@Number_Sisters", child.NumberOfSisters);
                cmd.Parameters.AddWithValue("@Disability_Status", child.DisabilityStatus);
                cmd.Parameters.AddWithValue("@Location_Code_ID", child.LocationCodeID);
                cmd.Parameters.AddWithValue("@NickName", child.OtherNameGoesBy);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(child.ChildID))
                        child.ChildID = reader["Child_ID"].ToString();
                    returnMessages.Add(new ReturnMessage(reader["Child_ID"].ToString(), "Child_ID", "Success! Enrollment save completed. All current progress has been saved."));
                }
                Con.Close();

                try
                {
                    // Clear is called so that only the current codes are attached to the child
                    SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Clear_Child_Codes]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    cmnd.Parameters.AddWithValue("@End_Date", DateTime.Now);
                    Con.Open();
                    cmnd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    returnMessages.Add(new ReturnMessage("-3", "ERROR", "SaveTheChild failed: " + ex.Message));
                }
                finally
                {
                    Con.Close();
                }

                if (child.PersonalityTypeID != 0)
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Add_Child_Personality_Type]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Personality_Type_Code_ID", child.PersonalityTypeID);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-4", "ERROR", "SaveTheChild failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }

                if (child.FavoriteActivitieIDs != null)
                    if (child.FavoriteActivitieIDs.Count > 0)
                        foreach (int ActivityID in child.FavoriteActivitieIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Add_Child_Favorite_Activity]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Favorite_Activity_Code_ID", ActivityID);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-5", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }

                if (child.ChoreIDs != null)
                    if (child.ChoreIDs.Count > 0)
                    {
                        foreach (int Chore in child.ChoreIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Add_Child_Chore]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Chore_Code_ID", Chore);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-6", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }

                if (child.ConfirmedNonDups != null)
                    if (child.ConfirmedNonDups.Count > 0)
                    {
                        foreach (string childid in child.ConfirmedNonDups)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Add_Child_Non_Duplicates]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID_1", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Child_ID_2", childid);
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-7", "ERROR", "SaveTheChild failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                Con.Close();
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "SaveTheChild failed: " + ex.Message));
            }

            // save non dup list

            return returnMessages;
        }


        public List<ReturnMessage> UploadEnrollmentFile(UploadFile file)
        {

            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmd = UploadEnrollmentFile_Database_Processing(file, Con);

                Con.Open();

                cmd.ExecuteNonQuery();

                returnMessages.Add(new ReturnMessage("0", "Success", "Success! File has successfully been saved."));
            }
            catch (Exception ex)
            {
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "UploadFile failed: " + ex.Message));
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return returnMessages;
        }

        private static SqlCommand UploadEnrollmentFile_Database_Processing(UploadFile file, SqlConnection Con)
        {
            SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Add_Child_File]", Con);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@User_ID", file.UserID);

            cmd.Parameters.AddWithValue("@Child_ID", file.ChildID);

            cmd.Parameters.AddWithValue("@File", file.FileBytes);

            cmd.Parameters.AddWithValue("@FileName", file.FileName);

            cmd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);

            cmd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);

            //cmd.Parameters.AddWithValue("@Stream_ID", file.FileStreamID);

            cmd.Parameters.AddWithValue("@Primary_Stream_ID", file.FileStreamID);

            if (file.MetaData != null && file.MetaData != new FileMetaData())
            {
                cmd.Parameters.AddWithValue("@Make", file.MetaData.Make);

                cmd.Parameters.AddWithValue("@Model", file.MetaData.Model);

                cmd.Parameters.AddWithValue("@Software", file.MetaData.Software);

                cmd.Parameters.AddWithValue("@DateTime", file.MetaData.DateTaken);

                cmd.Parameters.AddWithValue("@DateTimeOriginal", file.MetaData.Original);

                cmd.Parameters.AddWithValue("@DateTimeDigitized", file.MetaData.Digitized);

                cmd.Parameters.AddWithValue("@GPSVersionID", file.MetaData.GPSVersionID);

                cmd.Parameters.AddWithValue("@GPSLatitudeRef", file.MetaData.GPSLatitudeRef);

                cmd.Parameters.AddWithValue("@GPSLatitude", file.MetaData.GPSLatitude);

                cmd.Parameters.AddWithValue("@GPSLongitudeRef", file.MetaData.GPSLongitudeRef);

                cmd.Parameters.AddWithValue("@GPSLongitude", file.MetaData.GPSLongitude);

                cmd.Parameters.AddWithValue("@GPSAltitudeRef", file.MetaData.GPSAltitudeRef);

                cmd.Parameters.AddWithValue("@GPSAltitude", file.MetaData.GPSAltitude);

                cmd.Parameters.AddWithValue("@GPSTimeStamp", file.MetaData.GPSTimeStamp);

                cmd.Parameters.AddWithValue("@GPSImgDirectionRef", file.MetaData.GPSImgDirectionRef);

                cmd.Parameters.AddWithValue("@GPSDateStamp", file.MetaData.GPSDateStamp);
            }

            return cmd;
        }

        public List<ListChild> GetIncompleteEnrollments(string userid, int countryid, int locationid, int actionid)
        {
            List<ListChild> children = new List<ListChild>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Get_Saved_Enrollments]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (countryid > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", countryid);
                if (locationid > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                if (actionid > 0)
                    cmd.Parameters.AddWithValue("@Action_ID", actionid);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    children.Add(new ListChild(reader["Child_ID"].ToString(), "", reader["Location_Code_ID"].ToString(), reader["First_Name"].ToString(), reader["Middle_Name"].ToString(), reader["Last_Name"].ToString(), reader["Nickname"].ToString(), DateTime.Parse(reader["Date_of_Birth"].ToString()), reader["Gender_Code_ID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                ListChild error = new ListChild();
                error.FirstName = "ERROR";
                error.ChildID = ex.Message;
                children.Add(error);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return children;
        }

        public EnrollChild GetIncompleteChildEnrollment(string childid, string userid)
        {
            EnrollChild child = new EnrollChild();
            child.ChildID = childid;

            //Add db connection and procesing here. 
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Get_A_Saved_Enrollment]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    child.FirstName = reader["First_Name"].ToString();
                    child.MiddleName = reader["Middle_Name"].ToString();
                    child.LastName = reader["Last_Name"].ToString();
                    child.DateOfBirth = DateTime.Parse(reader["Date_of_Birth"].ToString());
                    if (!string.IsNullOrEmpty(reader["Grade_Level_Code_ID"].ToString()))
                        child.GradeLevelCodeID = int.Parse(reader["Grade_Level_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Health_Status_Code_ID"].ToString()))
                        child.HealthStatusCodeID = int.Parse(reader["Health_Status_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Lives_With_Code_ID"].ToString()))
                        child.LivesWithCodeID = int.Parse(reader["Lives_With_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Favorite_Learning_Code_ID"].ToString()))
                        child.FavoriteLearningCodeID = int.Parse(reader["Favorite_Learning_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Gender_Code_ID"].ToString()))
                        child.GenderCodeID = int.Parse(reader["Gender_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(reader["Number_Brothers"].ToString()))
                        child.NumberOfBrothers = int.Parse(reader["Number_Brothers"].ToString());
                    if (!string.IsNullOrEmpty(reader["Number_Sisters"].ToString()))
                        child.NumberOfSisters = int.Parse(reader["Number_Sisters"].ToString());
                    if (!string.IsNullOrEmpty(reader["Disability_Status"].ToString()))
                        child.DisabilityStatus = bool.Parse(reader["Disability_Status"].ToString());
                    if (!string.IsNullOrEmpty(reader["Location_Code_ID"].ToString()))
                        child.LocationCodeID = int.Parse(reader["Location_Code_ID"].ToString());
                    child.OtherNameGoesBy = reader["Nickname"].ToString();
                }
                Con.Close();

                try
                {
                    SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Get_Enrollment_Personality_Type]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.PersonalityTypeID = int.Parse(subreader["Personality_Type_Code_ID"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GetIncompleteChildEnrollment failed (2)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }

                try
                {
                    SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Get_Enrollment_Favorite_Activities]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.FavoriteActivitieIDs.Add(int.Parse(subreader["Favorite_Activity_Code_ID"].ToString()));
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GetIncompleteChildEnrollment failed (3)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }

                try
                {
                    SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Get_Enrollment_Chores]", Con);
                    cmnd.CommandType = CommandType.StoredProcedure;
                    cmnd.Parameters.AddWithValue("@User_ID", userid);
                    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                    Con.Open();
                    SqlDataReader subreader = cmnd.ExecuteReader();
                    while (subreader.Read())
                    {
                        child.ChoreIDs.Add(int.Parse(subreader["Chore_Code_ID"].ToString()));
                    }
                }
                catch (Exception ex)
                {
                    child.FirstName = "ERROR";
                    child.MiddleName = "GetIncompleteChildEnrollment failed (4)";
                    child.LastName = ex.Message;
                }
                finally
                {
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
                child.FirstName = "ERROR";
                child.MiddleName = "GetIncompleteChildEnrollment failed (1)";
                child.LastName = ex.Message;
            }
            return child;
        }


        public List<DownLoadFile> GetEnrollmentFile(DownLoadFile file)
        {
            List<DownLoadFile> files = new List<DownLoadFile>();

            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmnd = new SqlCommand("[Enrollment].[usp_Get_File]", Con);

                cmnd.CommandType = CommandType.StoredProcedure;

                Set_Enrollment_File_Params(file, cmnd);

                Con.Open();

                SqlDataReader reader = cmnd.ExecuteReader();

                while (reader.Read())
                {
                    Content_Files.Add_Download_File
                        (
                        files, 
                        reader,
                        new Guid(file.ChildID),
                        new Guid(file.UserID)
                        );
                }
            }
            catch (Exception ex)
            {
                file.ChildID = "ERROR";

                file.UserID = "GetFile failed (2)";

                file.FileName = ex.Message;

                files.Add(file);
            }
            finally
            {
                Con.Close();
            }

            return files;
        }



        private static void Set_Enrollment_File_Params(DownLoadFile file, SqlCommand cmnd)
        {
            cmnd.Parameters.AddWithValue("@User_ID", file.UserID);

            if (!string.IsNullOrEmpty(file.ChildID))
                cmnd.Parameters.AddWithValue("@Child_ID", file.ChildID);

            if (file.FileTypeID > 0)
                cmnd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);

            if (file.ContentTypeID > 0)
                cmnd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);

            if (file.FileID > 0)
                cmnd.Parameters.AddWithValue("@File_Data_ID", file.FileID);
            else
                cmnd.Parameters.AddWithValue("@Current", 1);
        }


        public List<ReturnMessage> DeleteAnEnrollment(string ChildID, string UserID)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Delete_Enrollment]", Con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                cmd.Parameters.AddWithValue("@Child_ID", ChildID);
                Con.Open();
                cmd.ExecuteNonQuery();
                returnMessages.Add(new ReturnMessage("0", "Success", "Success! Record has successfully been removed."));
            }
            catch (Exception ex)
            {
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "Delete Enrollment failed: " + ex.Message));
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return returnMessages;
        }

        public List<ReturnMessage> DeleteEnrollmentFile(UploadFile file)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Enrollment].[usp_Delete_File]", Con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", file.UserID);
                cmd.Parameters.AddWithValue("@Child_ID", file.ChildID);
                //cmd.Parameters.AddWithValue("@File", file.FileBytes);
                //cmd.Parameters.AddWithValue("@FileName", file.FileName);
                if (file.ContentTypeID > 0)
                    cmd.Parameters.AddWithValue("@Content_Type_ID", file.ContentTypeID);
                if (file.FileTypeID > 0)
                    cmd.Parameters.AddWithValue("@File_Type_ID", file.FileTypeID);
                if (!string.IsNullOrEmpty(file.FileStreamID))
                    cmd.Parameters.AddWithValue("@stream_id", file.FileStreamID);

                Con.Open();
                cmd.ExecuteNonQuery();
                returnMessages.Add(new ReturnMessage("0", "Success", "Success! File has successfully been removed."));
            }
            catch (Exception ex)
            {
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "UploadFile failed: " + ex.Message));
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return returnMessages;
        }


        /*
        public List<ReturnMessage> UploadEnrollmentFile_Profile_Image
            (
            string UserID,
            UploadFile file
            )
        {

            string the_Content_Type = "Profile";

            return Content_Files.Upload_Content_File  // UploadEnrollmentFile_Multi_Type
            (
                UserID,
                file,
                the_Content_Type,
                Classes.Settingz.enum_Operation_Type.Enrollment
            );

        }
        */





        /*
        public List<ReturnMessage> UploadEnrollmentFile_Consent
            (
            string UserID,
            UploadFile file
            )
        {

            string the_Content_Type = "Consent";

            return Content_Files.Upload_Content_File  // UploadEnrollmentFile_Multi_Type
            (
                UserID,
                file,
                the_Content_Type,
                Classes.Settingz.enum_Operation_Type.Enrollment
            );

        }

        public List<ReturnMessage> UploadEnrollmentFile_Action
            (
            string UserID,
            UploadFile file
            )
        {

            string the_Content_Type = "Action";

            return Content_Files.Upload_Content_File  // UploadEnrollmentFile_Multi_Type
            (
                UserID,
                file,
                the_Content_Type,
                Classes.Settingz.enum_Operation_Type.Enrollment
            );

        }


        public List<ReturnMessage> UploadEnrollmentFile_Art
            (
            string UserID,
            UploadFile file
            )
        {

            string the_Content_Type = "Art";

            return Content_Files.Upload_Content_File  // UploadEnrollmentFile_Multi_Type
            (
                UserID,
                file,
                the_Content_Type,
                Classes.Settingz.enum_Operation_Type.Enrollment
            );

        }



        public List<ReturnMessage> UploadEnrollmentFile_Letter
            (
            string UserID,
            UploadFile file
            )
        {

            string the_Content_Type = "Letter";

            return Content_Files.Upload_Content_File  // UploadEnrollmentFile_Multi_Type
            (
                UserID,
                file,
                the_Content_Type,
                Classes.Settingz.enum_Operation_Type.Enrollment
            );

        }
        */

    }
}