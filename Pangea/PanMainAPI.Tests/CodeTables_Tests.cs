﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PanMainAPI.DAO;


namespace PanMainAPI.Tests
{

    [TestClass]
    public class CodeTables_Tests
    {

        [TestMethod]
        public void Decline_Reason_Codes_Get()
        {

            string user_id = Guid.NewGuid().ToString ();
           
            List<Data.Decline_Reason_Codes>  l_Decline_Reason_Codes_Get = CodeTables.GetDeclineReasonCodes(user_id);

            Assert.IsTrue(l_Decline_Reason_Codes_Get.Count > 0);

        }


        [TestMethod]
        public void Decline_Sub_Reason_Codes_Given_DeclineReasonCodeID_Get()
        {

            int? DeclineReasonCodeID = 5;

            Decline_Sub_Reason_Codes_Handler(DeclineReasonCodeID);

        }

        private static void Decline_Sub_Reason_Codes_Handler(int? DeclineReasonCodeID)
        {
            string user_id = Guid.NewGuid().ToString();

            List<Data.Decline_Sub_Reason_Codes> l_Decline_Sub_Reason_Codes_Get = CodeTables.GetDeclineSubReasonCodes(user_id, DeclineReasonCodeID);

            Assert.IsTrue(l_Decline_Sub_Reason_Codes_Get.Count > 0);
        }

        [TestMethod]
        public void Decline_Sub_Reason_Codes_All_Get()
        {

            int? DeclineReasonCodeID = null;

            Decline_Sub_Reason_Codes_Handler(DeclineReasonCodeID);

        }

    }


}
