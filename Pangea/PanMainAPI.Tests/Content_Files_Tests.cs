﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PanMainAPI.DAO;
using PanMainAPI.Tests.Models;
using System;
using System.Collections.Generic;

namespace PanMainAPI.Tests
{
    [TestClass]
    public class Content_Files_Tests
    {

        public Process_Content_File_Upload_Params Get_Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type e_Operation_Type)
        {
            Process_Content_File_Upload_Params the_data = new Process_Content_File_Upload_Params (e_Operation_Type);

            the_data.UserID = Guid.NewGuid();

            return the_data;
        }


        private static void Upload_Content_File
            (
            Guid UserID,
            Classes.Settingz.enum_Operation_Type e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type e_Content_Binary_Type
            )
        {

            Guid the_ChildID = Guid.NewGuid();

            int file_type = 0;

            byte[] the_data = { 1, 2, 3 };

            UploadFile file =
                new UploadFile
                (
                    the_ChildID.ToString(),
                    file_type,
                    the_data,
                    UserID.ToString()
                );

            file.FileName = "testing.test";

            List<ReturnMessage> the_Result =
                Content_Files.Process_Content_File_Upload
                (
                    UserID.ToString (),
                    file,
                    e_Operation_Type,
                    e_Content_Binary_Type
                    );

            Assert.IsTrue(file.ContentTypeID > 0, "ContentTypeID Not Set");

            DownLoadFile dfile = new DownLoadFile(the_ChildID.ToString(), UserID.ToString());

            int i_records = 0;

            switch (e_Operation_Type)
            {
                case Classes.Settingz.enum_Operation_Type.Enrollment:

                    i_records =
                        Content_Files.ContentFile_Recs_Given_Child_ID_And_ContentTypeID_Count
                        (
                            dfile,
                            the_ChildID,
                            UserID,
                            file.ContentTypeID,
                            Classes.Settingz.enum_Operation_Type.Enrollment
                            );

                    break;

            }

            Assert.AreEqual(i_records, 1, "Didn't find WebAPI binary uploaded image in DB after add binary attempt");

        }



        [TestMethod]
        public void UploadEnrollmentFile_Profile_Image()
        {

            Process_Content_File_Upload_Params the_test_data_start = 
                Get_Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type.Enrollment);

            Upload_Content_File
            (
            the_test_data_start.UserID,
            the_test_data_start.e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type.Profile_Image
            );

        }


        [TestMethod]
        public void UploadEnrollmentFile_Consent()
        {

            Process_Content_File_Upload_Params the_test_data_start = Get_Process_Content_File_Upload_Params( Classes.Settingz.enum_Operation_Type.Enrollment);

            Upload_Content_File
            (
            the_test_data_start.UserID,
            the_test_data_start.e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type.Consent
            );

        }




        [TestMethod]
        public void UploadEnrollmentFile_Action()
        {

            Process_Content_File_Upload_Params the_test_data_start = Get_Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type.Enrollment);

            Upload_Content_File
            (
            the_test_data_start.UserID,
            the_test_data_start.e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type.Action
            );

        }



        [TestMethod]
        public void UploadEnrollmentFile_Art()
        {

            Process_Content_File_Upload_Params the_test_data_start = Get_Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type.Enrollment);

            Upload_Content_File
            (
            the_test_data_start.UserID,
            the_test_data_start.e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type.Art
            );

        }



        [TestMethod]
        public void UploadEnrollmentFile_Letter()
        {

            Process_Content_File_Upload_Params the_test_data_start = Get_Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type.Enrollment);

            Upload_Content_File
            (
            the_test_data_start.UserID,
            the_test_data_start.e_Operation_Type,
            Classes.Settingz.enum_Content_Binary_Type.Letter
            );

        }



    }


}
