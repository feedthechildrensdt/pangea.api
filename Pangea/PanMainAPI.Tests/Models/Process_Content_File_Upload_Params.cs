﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanMainAPI.Tests.Models
{

    public class Process_Content_File_Upload_Params
    {

        public Guid UserID;

        public Classes.Settingz.enum_Operation_Type e_Operation_Type;

        public Process_Content_File_Upload_Params(Classes.Settingz.enum_Operation_Type e_Operation_Type)
        {

            switch (e_Operation_Type)
            {
                case Classes.Settingz.enum_Operation_Type.Enrollment:

                    e_Operation_Type = Classes.Settingz.enum_Operation_Type.Enrollment;

                    break;

                case Classes.Settingz.enum_Operation_Type.Update:

                    e_Operation_Type = Classes.Settingz.enum_Operation_Type.Update;

                    break;
            }


        }
        
    }

}
